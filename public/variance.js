var when;
var unacknowledged_alarms = false; 
var stewards = {
    'adh': "Julie",
    'bme': "Sally",
    'bur': "Rafael",
    'cba': "Michael",
    'dcp': "Jesse",
    'ecj': "Derrick",
    'fac': "Rafael",
    'gwb': "Chris",
    'ltd': "Julie",
    'mnc': "Sally",
    'nez': "Rafael",
    'rsc': "Michael",
    'sac': "Jesse",
    'szb': "Derrick",
    'utc': "Chris"
};
var refresh_interval; // seconds
switch(environment) {
  case "development":
	refresh_interval = 5 * 1000; // seconds
    break;
  case "staging":
	refresh_interval = 30 * 1000; // seconds
    break;
  case "production":
	refresh_interval = 30 * 1000; // seconds
    break;
  default:
    console.log("no environment set");
}

var check_gui_continuously; 

function denormalize(utility_variances){
	var new_utility_variance = [];
	for(key in utility_variances){
		variance_new = {};
		variance_new.name = key;
		variance_new.y = utility_variances[key];
		variance_new.drilldown = key;
		new_utility_variance.push(variance_new);
	}
	utility_variances = new_utility_variance;
	return utility_variances;
}
function filter_for_utility(utility){
	var extracted = []
	for(energy of energies){
		if(energy.utility == utility){
			extracted.push(energy);
		} 
	}
	return extracted;
}
function filter_for_positive(energies){
	var extracted = []
	for(energy of energies){
		if(energy.variance > 0){
			extracted.push(energy);
		} 
	}
	return extracted;
}
function filter_for_negative(energies){
	var extracted = []
	for(energy of energies){
		if(energy.variance < 0){
			extracted.push(energy);
		} 
	}
	return extracted;
}
function get_utilities(energies){
	var utilities_set = new Set();
	for(energy of energies){
		utilities_set.add(energy.utility);
	}
	return utilities_set;
}
function get_utility_variances(energies){
	var utilities = Array.from(get_utilities(energies));
	var variances = {}
	for(utility of utilities){
		variances[utility] = 0;
	}
	for(energy of energies){
		for(utility of utilities){
			if(energy.utility == utility){
				var variance_total = variances[utility];
				variance_total = variance_total + energy.variance;
				variances[utility] = variance_total;
			}
		}
	}
	return variances
}
function normalize(energies){
	for(energy of energies){
		energy.energy = JSON.parse(energy.energy);
		energy.predicted = JSON.parse(energy.predicted);
		var midpoint = energy.when.indexOf("T");
		var date_portion = energy.when.substring(0,midpoint);
		energy.when = date_portion + "T06:00Z";
		energy.when = new Date(energy.when);
		energy.variance = (energy.energy - energy.predicted) / energy.predicted * 100;
	}
	return energies;
}
function denormalize_by_building(energies){
	var utilities_set = get_utilities(energies);
	var data_points = {};
	var data_point_utility = [];
	for(energy of energies){
		if(utilities_set.has(energy.utility)){
			var data_point = [];
			data_point.push(energy.building);
			data_point.push(energy.variance);
			var data_point_utility = data_points[energy.utility];
			if(data_point_utility == undefined){
				data_point_utility = [];
			}
			data_point_utility.push(data_point);
			data_points[energy.utility] = data_point_utility;
		}
	}
	return data_points;
}
function get_utility_variances_by_building(energies){
	var drilldown_data = [];
	var utilities = Array.from(get_utilities(energies));
	for(utility of utilities){
		var series = {};
		series.name = utility;
		series.id = utility;
		var utility_variances_by_building = denormalize_by_building(energies)[utility];
		series.data = utility_variances_by_building;
		drilldown_data.push(series);
	}
	return drilldown_data;
}
function filter_for_threshold(energies){
	var filtered_energies = [];
	var threshold = JSON.parse($("#threshold_slider").text());
	for(energy of energies){
		if(threshold <= Math.abs(energy.variance)){
			filtered_energies.push(energy);
		}
	}
	return filtered_energies;
}
function filter_utility_for_threshold(utility_variances){
	var new_utility_variance = {};
	var threshold = JSON.parse($("#threshold_slider").text());
	for(key in utility_variances){
		if(threshold <= Math.abs(utility_variances[key])){
			new_utility_variance[key] = utility_variances[key];
		}
	}
	return new_utility_variance;
}
function variance(when, mid, to){
	var parameter_when = when.substring(0,when.indexOf("T"));
	var uri = "/variance_for_buildings?when="+parameter_when;
	$.get(alarm_service + encodeURI(uri), function(data, status){
		data = JSON.parse(data);
		var energies = normalize(data);
		energies = filter_for_threshold(energies);
		if($("input[name='visualization_type']:checked").val() =="table"){
			var tbody0 = $("#tbody0").empty();
			for(energy of energies){
				var steward = stewards[energy.building];
				var row = $('<tr></tr>');
				if(energy.energy > energy.predicted){
					row.css("background-color","rgb(255,0,0, 0.1");
				} else {
					row.css("background-color","hsl(174,63%,90%)");
				}
	            var column = $('<td></td>').text(energy.when.toDateString("en-US")); 
	            row.append(column);  
	            var column = $('<td></td>').text(energy.building); 
	            row.append(column); 
	            var column = $('<td></td>').text(energy.utility); 
	            row.append(column);  
	            var column = $('<td></td>').text(Math.round(energy.predicted)); 
	            row.append(column); 
	            var column = $('<td></td>').text(Math.round(energy.energy));
	            row.append(column);
	            var column = $('<td></td>').text(Math.round(energy.variance)); 
	            row.append(column);
	            var column = $('<td></td>').text(steward); 
	            row.append(column);  
	            tbody0.append(row);
			}
			$("#table_section").css("display","block");
			$("#chart_section").css("display","none");
		} else {
			utility_variances = denormalize(get_utility_variances(energies));
			var utility_variances_by_building = get_utility_variances_by_building(energies);
			Highcharts.chart('chart0', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    subtitle: {
			        text: ''
			    },
			    xAxis: {
			        type: 'category'
			    },
			    yAxis: {
			        title: {
			            text: 'Energy'
			        }
			    },
			    legend: {
			        enabled: false
			    },
			    plotOptions: {
			        series: {
			            borderWidth: 0,
			            dataLabels: {
			                enabled: true,
			                format: '{point.y:.0f}%'
			            }
			        }
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:11px">{series.name}x</span><br>',
			        pointFormat: '<span style="color:{point.color}">{point.name}y: <b>{point.y:.2f}%</b></span>'
			    },
			    series: [
			        {
			            name: "Utilities",
			            colorByPoint: true,
			            data: utility_variances
			        }
			    ],
			    drilldown: {
			        series: utility_variances_by_building
			    }
			});
			$("#table_section").css("display","none");	
			$("#chart_section").css("display","block");	
		}
		var threshold = JSON.parse($("#threshold_slider").text());
		$("#threshold_value_holder").text(threshold);
		$("#output_section").css("display","block");
	});
}
function variance_handler(){
	variance(when.toJSON());
}
function gui_handler(){
	$("#output_section")
}
$(document).ready(function(){
	$("#to_date").datepicker({
		autoClose: true,
		onSelect: function(date_choosen){
			when = date_choosen;
		}
	});
	$("#threshold_slider").mouseup(variance_handler);
	$("#go_button").mouseup(variance_handler);
	threshold_low = 0.2;
	threshold_high = 0.2;
    clearTimeout(check_gui_continuously); 
    check_gui_continuously = setInterval(gui_handler, 2000);
    $("input[type='radio']").click(variance_handler);
	var slider = document.getElementById('threshold_slider');
	noUiSlider.create(slider, {
		start: [5],
		connect: true,
		step: 1,
		orientation: 'horizontal', // 'horizontal' or 'vertical'
		range: {
		 'min': 0,
		 'max': 25
		},
		format: wNumb({
		 decimals: 0
		})
	});
});