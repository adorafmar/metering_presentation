'use strict';

const e = React.createElement;

class Hello extends React.Component {
    render() {
        return "<h1>Hello world!</h1>";
    }
}

const domContainer = document.querySelector('#like_button_container');
ReactDOM.render("<Hello />", domContainer);