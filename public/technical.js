
let audio = new Audio("https://www.freesound.org/data/previews/178/178032_1449999-lq.mp3")
var prependColumnsAlarms =  ['At','Tag','Because', 'Acknowledged'];
var unacknowledged_alarms = false; 
var prependColumnsHistory = ['At','Tag','Became','Because'];
var oldNewsAfter = 5 * 60 * 1000; // (milliseconds)
var stateChangedAt = {};
var stateChangedPreviouslyAt = {};
var buildingAlarmStates = {};
var buildingLabels = {};
let labelZoomLimit = 14; 
let initialZoom = 15; 
var showMarkers = {};
var markers = {};
var buildings = {
    'msb': { 'lat': 30.2827, 'lon': -97.7257 },
    'ua9': { 'lat': 30.2903, 'lon': -97.7387 },
    'bwy': { 'lat': 30.2908, 'lon': -97.7382 },
    'sw7': { 'lat': 30.2908, 'lon': -97.7363 }, 
    'tsg': { 'lat': 30.2913, 'lon': -97.7386 },
    'aca': { 'lat': 30.2877, 'lon': -97.7357 },
    'adh': { 'lat': 30.2915, 'lon': -97.7406 },
    'and': { 'lat': 30.2882, 'lon': -97.7398 },
    'ahg': { 'lat': 30.2885, 'lon': -97.7377 },
    'anb': { 'lat': 30.2783, 'lon': -97.731 },
    'art': { 'lat': 30.2862, 'lon': -97.733 },
    'wat': { 'lat': 30.2784, 'lon': -97.7336 },
    'att': { 'lat': 30.282, 'lon': -97.7405 },
    'afp': { 'lat': 30.2853, 'lon': -97.7263 },
    'bsb': { 'lat': 30.2814, 'lon': -97.7354 },
    'btl': { 'lat': 30.2854, 'lon': -97.7403 },
    'bat': { 'lat': 30.2848, 'lon': -97.7389 },
    'jes': { 'lat': 30.2829, 'lon': -97.7368 },
    'bmc': { 'lat': 30.2902, 'lon': -97.7408 },
    'ben': { 'lat': 30.284, 'lon': -97.739 },
    'brb': { 'lat': 30.2852, 'lon': -97.7367 },
    'bot': { 'lat': 30.287, 'lon': -97.74 },
    'bio': { 'lat': 30.2872, 'lon': -97.7398 },
    'bgh': { 'lat': 30.2869, 'lon': -97.7388 },
    'bme': { 'lat': 30.2892, 'lon': -97.7386 },
    'grg': { 'lat': 30.2877, 'lon': -97.7399 },
    'bld': { 'lat': 30.2887, 'lon': -97.7394 },
    'bhd': { 'lat': 30.2831, 'lon': -97.736 },
    'brg': { 'lat': 30.2809, 'lon': -97.7362 },
    'bur': { 'lat': 30.2888, 'lon': -97.7385 },
    'cal': { 'lat': 30.2845, 'lon': -97.7402 },
    'crd': { 'lat': 30.2887, 'lon': -97.7401 },
    'clk': { 'lat': 30.2817, 'lon': -97.7349 },
    'cs3': { 'lat': 30.2807, 'lon': -97.7355 },
    'cs4': { 'lat': 30.2887, 'lon': -97.7325 },
    'cs5': { 'lat': 30.2907, 'lon': -97.7355 },
    'cs6': { 'lat': 30.2864, 'lon': -97.7358 },
    'cpe': { 'lat': 30.2903, 'lon': -97.7361 },
    'cdl': { 'lat': 30.2788, 'lon': -97.733 },
    'cba': { 'lat': 30.2842, 'lon': -97.7378 },
    'cda': { 'lat': 30.283, 'lon': -97.7257 },
    'cml': { 'lat': 30.2829, 'lon': -97.7253 },
    'csa': { 'lat': 30.2885, 'lon': -97.7357 },
    'ccg': { 'lat': 30.2821, 'lon': -97.7404 },
    'ccj': { 'lat': 30.2881, 'lon': -97.7304 },
    'cee': { 'lat': 30.2906, 'lon': -97.7364 },
    'ct1': { 'lat': 30.2866, 'lon': -97.7348 },
    'crh': { 'lat': 30.2885, 'lon': -97.7334 },
    'fc9': { 'lat': 30.2838, 'lon': -97.724 },
    'std': { 'lat': 30.2836, 'lon': -97.7323 },
    'dcp': { 'lat': 30.2762, 'lon': -97.733 },
    'dev': { 'lat': 30.2873, 'lon': -97.7236 },
    'dtb': { 'lat': 30.2873, 'lon': -97.7322 },
    'geb': { 'lat': 30.2863, 'lon': -97.7386 },
    'dfa': { 'lat': 30.2859, 'lon': -97.7318 },
    'eps': { 'lat': 30.2858, 'lon': -97.7367 },
    'eas': { 'lat': 30.2811, 'lon': -97.7382 },
    'etc': { 'lat': 30.2899, 'lon': -97.7354 },
    'ens': { 'lat': 30.2881, 'lon': -97.7353 },
    'ecj': { 'lat': 30.289, 'lon': -97.7354 },
    'utx': { 'lat': 30.2843, 'lon': -97.7344 },
    'win': { 'lat': 30.2859, 'lon': -97.7345 },
    'fc1': { 'lat': 30.2846, 'lon': -97.7226 },
    'fc2': { 'lat': 30.2839, 'lon': -97.7231 },
    'fc3': { 'lat': 30.2848, 'lon': -97.7237 },
    'fc4': { 'lat': 30.2843, 'lon': -97.7236 },
    'fc5': { 'lat': 30.2836, 'lon': -97.7246 },
    'fc6': { 'lat': 30.2834, 'lon': -97.7258 },
    'fc7': { 'lat': 30.2833, 'lon': -97.7264 },
    'fc8': { 'lat': 30.2854, 'lon': -97.7239 },
    'fct': { 'lat': 30.2843, 'lon': -97.7224 },
    'erc': { 'lat': 30.2769, 'lon': -97.7322 },
    'gar': { 'lat': 30.2852, 'lon': -97.7385 },
    'gdc': { 'lat': 30.2863, 'lon': -97.7365 },
    'szb': { 'lat': 30.2817, 'lon': -97.7388 },
    'gol': { 'lat': 30.2854, 'lon': -97.7412 },
    'gsb': { 'lat': 30.2842, 'lon': -97.7383 },
    'grf': { 'lat': 30.2842, 'lon': -97.7356 },
    'grc': { 'lat': 30.2838, 'lon': -97.7359 },
    'grp': { 'lat': 30.2844, 'lon': -97.7359 },
    'grs': { 'lat': 30.2836, 'lon': -97.736 },
    'gre': { 'lat': 30.284, 'lon': -97.7364 },
    'gug': { 'lat': 30.2795, 'lon': -97.7432 },
    'ppe': { 'lat': 30.2869, 'lon': -97.7359 },
    'ppa': { 'lat': 30.2869, 'lon': -97.7344 },
    'ppl': { 'lat': 30.2865, 'lon': -97.7352 },
    'hrc': { 'lat': 30.2843, 'lon': -97.7412 },
    'hma': { 'lat': 30.2869, 'lon': -97.7406 },
    'ipf': { 'lat': 30.2863, 'lon': -97.7265 },
    'int': { 'lat': 30.2884, 'lon': -97.7434 },
    'icb': { 'lat': 30.3148, 'lon': -97.7278 },
    'fdh': { 'lat': 30.2894, 'lon': -97.7325 },
    'pat': { 'lat': 30.288, 'lon': -97.7364 },
    'bma': { 'lat': 30.281, 'lon': -97.7374 },
    'jgb': { 'lat': 30.2859, 'lon': -97.7357 },
    'cma': { 'lat': 30.2894, 'lon': -97.7407 },
    'cmb': { 'lat': 30.2892, 'lon': -97.7411 },
    'jon': { 'lat': 30.2886, 'lon': -97.7317 },
    'jcd': { 'lat': 30.2822, 'lon': -97.7363 },
    'tcc': { 'lat': 30.287, 'lon': -97.729 },
    'jjf': { 'lat': 30.2835, 'lon': -97.7325 },
    'jhh': { 'lat': 30.2784, 'lon': -97.732 },
    'kin': { 'lat': 30.2904, 'lon': -97.7396 },
    'bel': { 'lat': 30.2837, 'lon': -97.7337 },
    'lth': { 'lat': 30.286, 'lon': -97.7352 },
    'fnt': { 'lat': 30.2879, 'lon': -97.7379 },
    'tsc': { 'lat': 30.2799, 'lon': -97.7335 },
    'cla': { 'lat': 30.2849, 'lon': -97.7354 },
    'lch': { 'lat': 30.2886, 'lon': -97.7408 },
    'ltd': { 'lat': 30.2893, 'lon': -97.7397 },
    'lfh': { 'lat': 30.2881, 'lon': -97.7408 },
    'lla': { 'lat': 30.2906, 'lon': -97.7405 },
    'llb': { 'lat': 30.2909, 'lon': -97.7405 },
    'llc': { 'lat': 30.2911, 'lon': -97.7405 },
    'lld': { 'lat': 30.2906, 'lon': -97.7409 },
    'lle': { 'lat': 30.2909, 'lon': -97.741 },
    'llf': { 'lat': 30.2911, 'lon': -97.7408 },
    'ldh': { 'lat': 30.2826, 'lon': -97.7359 },
    'lbj': { 'lat': 30.2858, 'lon': -97.7292 },
    'mai': { 'lat': 30.286, 'lon': -97.7394 },
    'mag': { 'lat': 30.2828, 'lon': -97.7309 },
    'gea': { 'lat': 30.2877, 'lon': -97.7392 },
    'mez': { 'lat': 30.2844, 'lon': -97.7389 },
    'mms': { 'lat': 30.2826, 'lon': -97.7301 },
    'mbb': { 'lat': 30.2885, 'lon': -97.7373 },
    'mnc': { 'lat': 30.2823, 'lon': -97.7327 },
    'mhd': { 'lat': 30.2836, 'lon': -97.7353 },
    'mrh': { 'lat': 30.2873, 'lon': -97.7309 },
    'nms': { 'lat': 30.2892, 'lon': -97.7376 },
    'nhb': { 'lat': 30.2876, 'lon': -97.738 },
    'nez': { 'lat': 30.2846, 'lon': -97.7325 },
    'noa': { 'lat': 30.2911, 'lon': -97.7375 },
    'nur': { 'lat': 30.2777, 'lon': -97.7336 },
    'fpc': { 'lat': 30.2794, 'lon': -97.725 },
    'par': { 'lat': 30.2848, 'lon': -97.7402 },
    'ttc': { 'lat': 30.2778, 'lon': -97.7348 },
    'pac': { 'lat': 30.2864, 'lon': -97.7311 },
    'pcl': { 'lat': 30.2828, 'lon': -97.7382 },
    'pob': { 'lat': 30.2869, 'lon': -97.7366 },
    'fac': { 'lat': 30.2863, 'lon': -97.7404 },
    'phr': { 'lat': 30.2882, 'lon': -97.7386 },
    'phd': { 'lat': 30.2824, 'lon': -97.7351 },
    'ppb': { 'lat': 30.2813, 'lon': -97.7269 },
    'hrh': { 'lat': 30.2842, 'lon': -97.7402 },
    'rsc': { 'lat': 30.2815, 'lon': -97.7324 },
    'sbs': { 'lat': 30.2805, 'lon': -97.725 },
    'mfh': { 'lat': 30.282, 'lon': -97.7311 },
    'wel': { 'lat': 30.2866, 'lon': -97.7377 },
    'rlm': { 'lat': 30.2889, 'lon': -97.7363 },
    'rhd': { 'lat': 30.2831, 'lon': -97.7351 },
    'sag': { 'lat': 30.2887, 'lon': -97.7427 },
    'sjg': { 'lat': 30.2877, 'lon': -97.7329 },
    'sjh': { 'lat': 30.2823, 'lon': -97.7344 },
    'sea': { 'lat': 30.29, 'lon': -97.7373 },
    'ssw': { 'lat': 30.2806, 'lon': -97.7327 },
    'ser': { 'lat': 30.2877, 'lon': -97.7346 },
    'srh': { 'lat': 30.285, 'lon': -97.7289 },
    'swg': { 'lat': 30.2912, 'lon': -97.7371 },
    'sac': { 'lat': 30.2849, 'lon': -97.7363 },
    'ssb': { 'lat': 30.2901, 'lon': -97.7384 },
    'sut': { 'lat': 30.285, 'lon': -97.7408 },
    'pai': { 'lat': 30.287, 'lon': -97.7387 },
    'sof': { 'lat': 30.2809, 'lon': -97.7275 },
    'tsb': { 'lat': 30.3152, 'lon': -97.7267 },
    'tmm': { 'lat': 30.287, 'lon': -97.7324 },
    'tnh': { 'lat': 30.2886, 'lon': -97.7307 },
    'trg': { 'lat': 30.2791, 'lon': -97.7339 },
    'dff': { 'lat': 30.2794, 'lon': -97.7265 },
    'unb': { 'lat': 30.2866, 'lon': -97.7411 },
    'uil': { 'lat': 30.2833, 'lon': -97.7236 },
    'upb': { 'lat': 30.2841, 'lon': -97.7304 },
    'uss': { 'lat': 30.2926, 'lon': -97.7363 },
    'utc': { 'lat': 30.2831, 'lon': -97.7388 },
    'uta': { 'lat': 30.2793, 'lon': -97.7428 },
    'wrw': { 'lat': 30.2875, 'lon': -97.7359 },
    'wag': { 'lat': 30.2851, 'lon': -97.7376 },
    'wwh': { 'lat': 30.2893, 'lon': -97.7418 },
    'wmb': { 'lat': 30.2854, 'lon': -97.7406 },
    'wch': { 'lat': 30.2861, 'lon': -97.7384 },
    'hsm': { 'lat': 30.2889, 'lon': -97.7408 }
}; 
var measurementTypesToPrism = {
    'CHW_DP': 'DP',
    'E_TBU_DMD': 'E_TBU_DMD',
    'ANOTHER_TAG': 'ANOTHER_TAG In Prism',
    'AND_ANOTHER': 'AND_ANOTHER In Prism',
};
var timerPullHistory;
var timerPullAlarmsText;
var timerAudible;
var measurementTypes = Object.keys(measurementTypesToPrism);
var relayEndPoint = "http://10.101.206.106:83/";
// var relayMachine = "10.101.206.13"; // production
var relayMachine = "localhost:83"; // development
var relayAlarmsEndPoint = "http://" + relayMachine
var relayHistoryEndPoint = "http://" + relayMachine + "/history";
var ignitionMachine = "10.101.206.9:8088" // production
var ignitionMachine = "localhost:8088" // development
var ignitionProject = "http://" + ignitionMachine + "/main/system/webdev/alarms/"; 
var ignitionAlarmsEndPoint = ignitionProject + "alarms"; // development
var ignitionShelveEndPoint = ignitionProject + "shelve"; // development

function showLabels(){
    if(map.getZoom() >= labelZoomLimit){
        Object.keys(buildings).forEach(function(building){
            if(typeof markers[building] != 'undefined'){
                var buildingLabel = buildingLabels[building];
                if (buildingLabel) buildingLabel.setMap(null);
                var position = buildings[building];
                var lat = position['lat']; 
                var lon = position['lon']; 
                if(buildingAlarmStates[building]){
                    if(showMarkers[building]){
                        if(map.getZoom() > labelZoomLimit){
                            buildingLabel = new TxtOverlay(new google.maps.LatLng(lat, lon), building.toUpperCase(), "building-name-close-style", map);
                        } else {
                            buildingLabel = new TxtOverlay(new google.maps.LatLng(lat, lon), building.toUpperCase(), "building-name-style", map);
                        }
                    }
                }
                buildingLabels[building] = buildingLabel;
            }
        }); 

    } else {
        Object.keys(buildingLabels).forEach(function(building){
            var buildingLabel = buildingLabels[building]; 
            if(typeof buildingLabel != 'undefined')
                buildingLabels[building].setMap(null);
        });            
    }    
}
function capitalizeFirst(string) 
{
	string = (string + ""); 
    return (string + "").charAt(0).toUpperCase() + string.slice(1);
}
function pullAlarmsMap(){ 
	console.log('pullAlarmsMap'); 
	$.get(relayEndPoint, function(data_string, status){
        var data = JSON.parse(data_string); 
        for(i=0; i != data.rows.length; i++){
        	var theRow = data.rows[i];
	        active = theRow[0];
			var nodeId = theRow[3].split('/'); 
			nodeId = nodeId[0] + ' ' + nodeId[1]; 
        	var acknowledged = theRow[1]; 
			var uuid = theRow[5]; 
		    var posBuilding = nodeId.indexOf('/') + 1; 
		    var building = nodeId.substring(posBuilding, posBuilding + 3).toLowerCase(); 
		    var sourcePieces = theRow[3].split('/');
        	var theSource = sourcePieces[1];
		    var constraint = theRow[4];
			constraint = constraint.substring(1,constraint.length-1); // remove {}
			constraint = constraint.split(', ');
			var constraintHash = {};
			for(elementN in constraint){
				var pieces = constraint[elementN].split('=');
				var lpiece = pieces[0];
				var rpiece = pieces[1];
				constraintHash[lpiece] = rpiece;
			}
			timeAck = constraintHash['eventTime'];
    		because = constraintHash['mode'] + " " + constraintHash['eventValue'];
			var measurementType = '(not found)'; 
		    for(var j=0; j != measurementTypes.length; j++){
		        var measurementTypePossible = measurementTypes[j]; 
		        if (nodeId.indexOf(measurementTypePossible) > -1){
		            measurementType = measurementTypePossible;
		        } 
		    }
		    if(typeof(measurementTypesToPrism[measurementType]) != 'undefined')
		    	var measurementTypeToPrism = measurementTypesToPrism[measurementType].toUpperCase();
		    process_tag(theRow);
        }    
		produce_table(); 
    });	
}

function produce_table(){
    if(Object.keys(buildingAlarmStates).length == 0){ 
        buildingAlarmCount = 0; 
    } else { 
        buildingAlarmCount = 0 + Object.values(buildingAlarmStates).reduce(function(accumulator, currentValue) { 
            return accumulator + currentValue; 
        }); 
    } 
    switch(buildingAlarmCount) {
    case 0:
        $("#outage-count").text('There are no power outages'); 
        $("#legend").css("visibility", "hidden"); 
        break;
    case 1:
        $("#outage-count").text('There is '  + buildingAlarmCount + ' power outage'); 
        $("#legend").css("visibility", "visible"); 
        break;
    default:
        $("#outage-count").text('There are '  + buildingAlarmCount + ' power outages'); 
        $("#legend").css("visibility", "visible"); 
    }
    function sortData0(a, b) {
        if (a[2] === b[2]) {
            return 0;
        }
        else {
            return (a[2] > b[2]) ? -1 : 1;
        }
    }
    var data0 = []; 
    Object.keys(buildingAlarmStates).forEach(function(building){ 
        var myRow = []; 

        myRow.push(building.toUpperCase());

        var humanState = buildingAlarmStates[building] == true ? "Down" : "Up"; 
        myRow.push(humanState);

        myRow.push(Date.parse(stateChangedAt[building]));

        data0.push(myRow);
    });
    data0.sort(sortData0); 

    var tableRegion = $("#data-region-table"); 
    tableRegion.empty(); 
    var table = $("<table>"); 
    var tableHeader = $("<thead>"); 
    var tableHeaderColumn = $("<th>").text("Building"); 
    tableHeader.append(tableHeaderColumn); 
    var tableHeaderColumn = $("<th>").text("Detected"); 
    tableHeader.append(tableHeaderColumn); 
    var tableHeaderColumn = $("<th>").text("Status"); 
    tableHeader.append(tableHeaderColumn); 
    var tableHeaderColumn = $("<th>").text("Since"); 
    tableHeader.append(tableHeaderColumn); 
    var tableBody = $("<tbody>"); 
    table.append(tableHeader); 
    table.append(tableBody); 
    tableRegion.append(table); 
    var tableData = $("#data-region-table > table > tbody"); 
    for(row in data0){
        let eventOccurredAt = new Date(data0[row][2]); 
        let alarming = data0[row][1] == 'Down' ? true : false; 
        var building = data0[row][0].toLowerCase(); 
        let oldNews = Date.now() > Date.parse(stateChangedAt[building]) + oldNewsAfter ? true : false; 
        if(!oldNews || oldNews && alarming){
            var trElement = $("<tr>");
            if(row % 2 == 0) 
                trElement.css("background-color", "ghostwhite"); 

            var tdElement = $("<td>"); 
            tdElement.addClass("table-building"); 
            tdElement.text(data0[row][0]);
            trElement.append(tdElement); 

            var tdElement = $("<td>"); 
            tdElement.css("color", "blue"); 
            var spanElement = $("<span>"); 
            spanElement.addClass("date-portion"); 
            spanElement.text(eventOccurredAt.toDateString());
            tdElement.append(spanElement); 
            var spanElement = $("<span>"); 
            spanElement.addClass("time-portion"); 
            spanElement.text(eventOccurredAt.toLocaleTimeString('en-GB'));
            tdElement.append(spanElement); 
            trElement.append(tdElement); 

            // status 
            var tdElement = $("<td>");
            var divElement = $("<div>"); 
            divElement.addClass("pill"); 
            divElement.css("text-align", "center"); 
            divElement.css("color", "white"); 
            if(data0[row][1] == "Up"){
                divElement.css("background-color", "#008000"); 
            } else {
                divElement.css("background-color", "#800000"); 
            }
            divElement.css("border-radius", "25px"); 
            divElement.text(data0[row][1]); 
            tdElement.append(divElement);
            trElement.append(tdElement); 

            var tdElement = $("<td>"); 
            tdElement.addClass("table-since"); 
            tdElement.css("text-align", "center"); 
            var currentTime = Date.now(); 
            var eventTime = data0[row][2];
            var elapasedTime = Math.round((currentTime - eventTime)/1000/60); 
            var pElement = $('<p>'); 
            pElement.addClass("unit"); 
            pElement.text(' mins ago'); 
            if(elapasedTime > 60){
                elapasedTime = elapasedTime / 60; 
                pElement.text(' hrs ago'); 
            }
            tdElement.text(elapasedTime.toFixed(0));
            tdElement.append(pElement); 
            trElement.append(tdElement); 

            tableData.append(trElement);
        }
    }
    if($(tableData.selector + " > tr").size() == 0){
        var noOutages = $("<p>").text("There are no power outages"); 
        tableRegion.append(noOutages); 
    }
}

function process_tag(data_ignition_form){
    // console.log("process_tag"); 
    /*  event-driven
	    {
			nodeId:"ns=2;s=[default]bur/SysErr"
			timestamp:"2017-12-20T21:03:48.906Z"
			value:false
		}
		state-driven
		[
			0:false
			1:true
			2:"Wed Dec 06 01:30:50 CST 2017"
			3:"CLA/SysErr/Alarm"
			4:"{mode=Not Equal, eventValue=false, activePipeline=SysErr/SysErrActive, systemAck=true, name=Alarm, eventTime=Wed Dec 06 01:30:50 CST 2017, ackNotesReqd=true}"
			5:"3a0abc17-39cb-44e2-94c7-33a9fdde4697"
			6:{isShelved goes here}
		]
	*/
    // spinner.stop(); 
    data = {}; 
    data.nodeId = data_ignition_form[3]; 
    data.timestamp = data_ignition_form[2]; 
    data.value = data_ignition_form[0] && !data_ignition_form[1] && !data_ignition_form[6]; 

    var nodeId = data.nodeId; 
    var building = nodeId.substring(0, 3).toLowerCase(); 
    if(buildings[building] == undefined){
    	// console.log(building + ' is a new building'); 
    	return; 
    }
    var currentAlarmState = data.value; //  == 1 ? true : false; 
    var previousAlarmState = buildingAlarmStates[building]; 
    if(typeof previousAlarmState == 'undefined'){ 
        stateChangedPreviouslyAt[building] = stateChangedAt[building]; 
        stateChangedAt[building] = data.timestamp; 
    } else { 
        if(currentAlarmState != previousAlarmState){ 
            stateChangedPreviouslyAt[building] = stateChangedAt[building]; 
            stateChangedAt[building] = data.timestamp; 
        } 
    }
    var position = buildings[building];
    var lat = position['lat']; 
    var lon = position['lon']; 
    var myLatLng = {lat: lat, lng: lon}; 
    var marker = markers[building]; 
    var icon; 

    if (marker) {
        marker.setMap(null);
    } 
    var okMarker = {
        path: 'M 13.224663,13.202077 A 13.229166,13.229166 0 0 1 -0.00450789,26.431237 13.229166,13.229166 0 0 1 -13.233667,13.202077 13.229166,13.229166 0 0 1 -0.00450789,-0.02709304 13.229166,13.229166 0 0 1 13.224663,13.202077 Z',
        fillColor: 'green',
        fillOpacity: 0.8,
        scale: 1,
        strokeColor: '#b1b1b1',
        strokeWeight: 2
    };
    var questionMarker = {
        path: 'M0 0c-1.88 0.291 -3.533 1.402 -4.479 3.048l-52.634 91.188c-1.101 1.914 -1.118 4.303 0 6.229c1.118 1.933 3.198 3.144 5.448 3.142h105.268c2.25 0.002 4.33 -1.209 5.449 -3.142c1.117 -1.93 1.1 -4.315 0 -6.229l-52.636 -91.188C5.123000000000047 0.7939999999999969 2.560000000000059 -0.402000000000001 0 0z',
        fillColor: 'yellow',
        fillOpacity: 0.8,
        scale: 0.4,
        strokeColor: '#b1b1b1',
        strokeWeight: 2
    };
    var bangMarker = {
        path: 'm -0.5261942,0.21632772 c -1.88,0.291 -3.533,1.40199998 -4.479,3.04799998 L -57.639194,94.45232 c -1.101,1.914 -1.118,4.303 0,6.229 1.118,1.933 3.198,3.144 5.448,3.142 H 53.076809 c 2.25,0.002 4.33,-1.209 5.449,-3.142 1.117,-1.93 1.1,-4.315 0,-6.229 L 5.8898078,3.2643277 c -1.293002,-2.254 -3.856002,-3.44999997 -6.416002,-3.04799998 z M 1.8849068,70.86549 h -1.968739 c -0.203671,-3.43962 -0.882556,-7.558152 -2.036631,-12.355573 l -2.342133,-9.775827 c -1.403017,-5.860922 -2.10452,-10.058639 -2.104512,-12.593166 -8e-6,-5.114154 2.477898,-7.67125 7.433706,-7.671317 2.172393,6.7e-5 3.965754,0.707227 5.380093,2.121498 1.414311,1.414376 2.1214742,3.219062 2.1214972,5.414037 -2.3e-5,2.330863 -0.7467852,6.573846 -2.2402892,12.728948 l -2.376082,9.775827 c -0.837293,3.439684 -1.459593,7.55819 -1.86691,12.355573 z m -0.91648,6.41536 c 2.308166,1e-5 4.095872,0.77507 5.363134,2.32516 1.26721,1.55012 1.9008342,3.2077 1.9008652,4.97278 -3.1e-5,2.33081 -0.7694302,4.12417 -2.3081992,5.38011 -1.538805,1.25591 -3.190738,1.88386 -4.9558,1.88386 -2.330827,0 -4.124189,-0.76939 -5.380102,-2.30817 -1.255931,-1.53878 -1.883888,-3.19072 -1.883879,-4.9558 -9e-6,-2.05926 0.695838,-3.79041 2.08754,-5.19342 1.391686,-1.40299 3.117167,-2.10451 5.176441,-2.10452 z',
        fillColor: 'red',
        fillOpacity: 0.8,
        scale: 0.4,
        strokeColor: 'black',
        strokeWeight: 1
    };
    var dotGreenMarker = {
        path: 'M -3.3395465,6.8620925 -0.82896543,9.1831958 3.386916,4.2567727 M 6.5923762,6.5203363 A 6.6135831,6.6135831 0 0 1 -0.02120924,13.133916 6.6135831,6.6135831 0 0 1 -6.6347892,6.5203363 6.6135831,6.6135831 0 0 1 -0.02120924,-0.09324887 6.6135831,6.6135831 0 0 1 6.5923762,6.5203363 Z',
        fillColor: '#e7ffe7',
        fillOpacity: 0.0,
        scale: 1,
        strokeColor: 'green',
        strokeWeight: 2,
        strokeOpacity: 0.25
    };
    var dotRedMarker = {
        path: 'm -3.8369261,9.6568904 6.9159405,-6.347507 m 0,6.347507 -6.9159402,-6.347507 M 8.2529812,6.3432583 A 8.8181108,8.8181108 0 0 1 -0.56513273,15.161365 8.8181108,8.8181108 0 0 1 -9.3832393,6.3432583 8.8181108,8.8181108 0 0 1 -0.56513273,-2.4748553 8.8181108,8.8181108 0 0 1 8.2529812,6.3432583 Z',
        fillColor: '#ffe5e6',
        fillOpacity: 1,
        scale: 0.8,
        strokeColor: 'red',
        strokeWeight: 2
    };
    var dotGrayMarker = {
        path: 'M 6.5923762,6.5203363 A 6.6135831,6.6135831 0 0 1 -0.02120924,13.133916 6.6135831,6.6135831 0 0 1 -6.6347892,6.5203363 6.6135831,6.6135831 0 0 1 -0.02120924,-0.09324887 6.6135831,6.6135831 0 0 1 6.5923762,6.5203363 Z',
        fillColor: 'gray',
        fillOpacity: 0.2,
        scale: 1,
        strokeColor: 'white',
        strokeWeight: 2
    };
    if (typeof currentAlarmState != 'undefined') {
        if(!currentAlarmState){
            icon = dotGreenMarker; // green
        } else { 
            icon = dotRedMarker; // red
        }
    } else {
        icon = dotGrayMarker; // yellow
    }
    var showMarker; 
    showMarker = true; 
    if(showMarker){
        marker = new google.maps.Marker({ 
            position: myLatLng, 
            map: map, 
            title: building.toUpperCase(), 
            icon: icon, 
        });
    }
    showMarkers[building] = showMarker; 

    markers[building] = marker; 

    buildingAlarmStates[building] = currentAlarmState; 
    showLabels(); 
}
function alarmStatusMap(){ 
    pullAlarmsMap(); 
    clearTimeout(pullAlarmsMap); 
    pullAlarmsMap = setInterval(pullAlarmsMap, 5000); 
} 
function alarmHistory(){ 
    setUpHistoryHeader(); 
    pullHistory();
} 
function createHistoryHeader(data){ 
    var row; 
    row = $('<tr/>'); 
    var th;
    for(i=0; i!=prependColumnsHistory.length; i++){
        th = $('<th/>').text(prependColumnsHistory[i]);
        row.append(th); 
    }
    $("#table_history thead").append(row); 
}
function setUpHistoryHeader(){ 
    $.get(relayHistoryEndPoint, function(data, status){
        $('#table_history > thead').empty(); 
        $('#table_history > tbody').empty(); 
        createHistoryHeader(data); 
        $("#table_history").tablesorter();  
        $("#table_history").trigger("update"); 
        var sorting = [[1,1]]; 
        // $("table").trigger("sorton",[sorting]);            
    }); 
}
function string_contains(string0, piece){
    return string0.indexOf(piece) != -1; 
}
function normalSource(theSource){
    var rv = true; 
    rv = rv && !string_contains(theSource, "evt:"); 
    rv = rv && !string_contains(theSource, "System Startup"); 
    return rv; 
}
function pullHistory(){ 
    console.log('pullHistory');
    $.get(relayHistoryEndPoint, function(data_string, status){ 
        $('#table_history > tbody').empty();  
        var data = JSON.parse(data_string); 
        for(i=0; i != data.rows.length; i++){ 
            var theRow = data.rows[i];
            var theSource = theRow[0];
            if(normalSource(theSource)){ 
                theSource = theSource.split(':'); 
                theSource = theSource[3].split('/')[0] + ' ' + theSource[3].split('/')[1]; 
                var activeData = theRow[1];
                var acknowledgedData = theRow[2];
                var clearData = theRow[3];
                var titleBeingUsed = $("#title").text(); 
                var filtering = titleBeingUsed  != "Alarm History"; 
                var filterFor = titleBeingUsed.split(' ')[0]; 
                var displayRow = theSource == filterFor; 
                // if(!filtering || filtering && displayRow){
                    var row = $('<tr/>'); 
                    var timeAck = '?'; 
                    var active = theRow[1].indexOf(',') != -1 ? 1 : 0;
                    var acknowledged = theRow[2].indexOf(',') != -1 ? 1 : 0;
                    var cleared = theRow[3].indexOf(',') != -1 ? 1 : 0;
                    var state = cleared * 4 + acknowledged * 2 + active; 
                    var because = '?'; 
                    var source = '?'; 
                    switch(state){ 
                        case 1: // Active
                            activeData = activeData.substring(1,activeData.length-1); // remove {}
                            activeData = activeData.split(', ');
                            var aHash = {};
                            for(elementN in activeData){
                                var pieces = activeData[elementN].split('=');
                                var lpiece = pieces[0];
                                var rpiece = pieces[1];
                                aHash[lpiece] = rpiece;
                            }
                            timeAck = aHash['eventTime'];
                            because = 'It became ' + aHash['eventValue'];
                            break;
                        case 2: 
                        case 3: // Acknowledged
                            acknowledgedData = acknowledgedData.substring(1,acknowledgedData.length-1); // remove {}
                            acknowledgedData = acknowledgedData.split(', ');
                            var aHash = {};
                            for(elementN in acknowledgedData){
                                var pieces = acknowledgedData[elementN].split('=');
                                var lpiece = pieces[0];
                                var rpiece = pieces[1];
                                aHash[lpiece] = rpiece;
                            }
                            timeAck = aHash['eventTime'];
                            var user = aHash['ackUser'].split(':')[1]; 
                            if(user == 'Live Event Limit'){
                                var reason = "it was automatically acknowledged"
                            } else {
                                var reason = aHash['ackNotes']
                            }
                            because = user + ' SAYS ' + reason;
                            break;
                        case 4: 
                        case 5: 
                        case 6: 
                        case 7: // Cleared
                            clearData = clearData.substring(1,clearData.length-1); // remove {}
                            clearData = clearData.split(', ');
                            var aHash = {};
                            for(elementN in clearData){
                                var pieces = clearData[elementN].split('=');
                                var lpiece = pieces[0];
                                var rpiece = pieces[1];
                                aHash[lpiece] = rpiece;
                            }
                            timeAck = aHash['eventTime'];
                            because = 'It became ' + aHash['eventValue'];
                            break; 
                        default:
                            timeAck = '(error)';
                    }; 
                    switch(state){
                        case 1:
                            var humanState = 'Active'; 
                            break; 
                        case 2:
                            var humanState = 'Acknowledged'; 
                            break; 
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            var humanState = 'Cleared'; 
                            break; 
                        default:
                            var humanState = '(error)'; 
                    } 
                    var td; 
                    td = $('<td/>')
                    td.text(timeAck);
                    row.append(td); 
                    td = $('<td/>').text(theSource);
                    row.append(td);
                    td = $('<td/>').text(humanState); 
                    td.css('border-radius', '25px');
                    td.css('text-align', 'center');
                    switch(state){
                        case 1:
                            td.css('color', 'white');
                            td.css('background-color', '#800000'); 
                            break; 
                        case 2:
                            td.css('color', 'black');
                            td.css('background-color', '#fcff00');
                            break; 
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            td.css('color', 'white');
                            td.css('background-color', '#008000'); 
                            break; 
                    } 
                    row.append(td); 
                    td = $('<td/>').text(because);
                    row.append(td); 
                //     for(j=0; j != data.columns.length; j++){
                //      var columnName = data.columns[j].toLowerCase(); 
                //      var posOfOneOfThese = $.inArray(data.columns[j].toLowerCase(), notTheseHistory);
                //      if(posOfOneOfThese == -1){
                //          var td = $('<td/>').text(theRow[j]);
                            // row.append(td);
                //      }
                //     }
                    $('#table_history > tbody').append(row);
                // }
            }
        };
        $("#table_history").trigger("update");
    }); 
}
function setUpAlarmHeader(){   
    $.get(relayAlarmsEndPoint, function(data, status){
        $('#table1 > thead').empty(); 
        var thead = $('<thead/>'); 
        var row; 
        row = $('<tr/>'); 
        var th; 
        for(i=0; i!=prependColumnsAlarms.length; i++){
            th = $('<th/>').text(prependColumnsAlarms[i]);
            row.append(th); 
        } 
        var trendButton = $('<input />', {type: 'button', value: 'trend'});
        var th  = $('<th/>').append(trendButton.prop('outerHTML')); 
        // row.append(th);

        $("#table1 thead").append(row);
        $("#table1").tablesorter();  
        $("#table1").trigger("update"); 
        var sorting = [[1,1]]; 
        // $("#table1").trigger("sorton",[sorting]); 
        $('input[type="button"][value="trend"]').click(function(){
            var prefix = 'https://energyportal.utilities.utexas.edu:4443/api/Charts/QuickTrend/Line/tags/';
            var checkBoxes = $('input[type="checkbox"]:checked');
            var tags = '';
            checkBoxes.each(function(index){
                var rowId = this.id.substring(5, this.id.length); 
                var buildingField = $('#building'+rowId);
                var measurementField = $('#measurement'+rowId);
                if(index == 0)
                    tags = buildingField.attr('value').toUpperCase() + '_' + measurementField.attr('value').toUpperCase()
                else
                    tags = tags + ',' + buildingField.attr('value').toUpperCase() + '_' + measurementField.attr('value').toUpperCase(); 
            });
            var url = prefix + tags;
            var w = window.innerWidth;
            var h = window.innerHeight;
            window.open(url, "_blank", "height=" + h + ",left=100,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,top=100,width=" + w*2/3);
        }); 
    }); 
}
function playAudible() { 
    audio.play()
    return audio.paused;
}
function pullAlarmsText(){ 
    console.log('pullAlarmsText'); 
    if(unacknowledged_alarms){
        if(timerAudible == undefined){ 
            timerAudible = setInterval(playAudible, 3000); 
        }
    } else {
        clearInterval(timerAudible); 
        timerAudible = undefined; 
    }
    $('input[type="button"][value="Submit"]').unbind('click'); 
    $.get(relayAlarmsEndPoint, function(data_string, status){ 
        if(data_string.indexOf("Expired") != -1)
            debugger; 
        var data = JSON.parse(data_string); 
        $('#table1 > tbody').empty(); 
        unacknowledged_alarms = false; 
        for(i=0; i != data.rows.length; i++){
            var theRow = data.rows[i];
            var active = theRow[0];
            var shelved = theRow[6];
            if(active && !shelved){
                var sourcePath = theRow[3]; 
                var nodeId = sourcePath.split('/'); 
                nodeId = nodeId[0] + ' ' + nodeId[1]; 
                var acknowledged = theRow[1]; 
                if(!acknowledged)
                    unacknowledged_alarms = true; 
                var uuid = theRow[5]; 
                var posBuilding = nodeId.indexOf('/') + 1; 
                var building = nodeId.substring(posBuilding, posBuilding + 3).toLowerCase(); 
                var sourcePieces = theRow[3].split('/');
                var theSource = sourcePieces[1];
                var constraint = theRow[4];
                constraint = constraint.substring(1,constraint.length-1); // remove {}
                constraint = constraint.split(', ');
                var constraintHash = {};
                for(elementN in constraint){
                    var pieces = constraint[elementN].split('=');
                    var lpiece = pieces[0];
                    var rpiece = pieces[1];
                    constraintHash[lpiece] = rpiece;
                }

                timeAck = constraintHash['eventTime'];
                because = constraintHash['mode'] + " " + constraintHash['eventValue'];

                var row = $('<tr/>'); 
                var td; 
                td = $('<td/>').text(timeAck);
                row.append(td); 
                td = $('<td/>').attr('id', 'source'+i).text(nodeId);
                row.append(td);
                td = $('<td/>').text(because);
                row.append(td);  
                td = $('<td/>').text(capitalizeFirst(acknowledged)); 
                td.css('border-radius', '25px');
                td.css('text-align', 'center');
                td.css('background-color', 'white'); 
                td.css('vertical-align', 'middle'); 
                if(acknowledged){
                    td.css('color', 'black');
                    td.css('background-color', '#fffe00'); 
                } else {
                    td.css('color', 'white');
                    td.css('background-color', '#800000');
                }
                row.append(td); 

                var trendCheckBox  = $('<input />', {type: 'checkbox', id: 'trend'+i});
                td = $('<td/>').append(trendCheckBox.prop('outerHTML')); 
                // row.append(td); 

                var measurementType = '(not found)'; 
                for(var j=0; j != measurementTypes.length; j++){
                    var measurementTypePossible = measurementTypes[j]; 
                    if (nodeId.indexOf(measurementTypePossible) > -1){
                        measurementType = measurementTypePossible;
                    } 
                }
                if(typeof(measurementTypesToPrism[measurementType]) != 'undefined')
                    var measurementTypeToPrism = measurementTypesToPrism[measurementType].toUpperCase();
                var td; 
                var ackButton  = $('<input />', {type: 'button', id: 'acknowledge'+i, value: 'acknowledge'});
                var shelveButton  = $('<input />', {type: 'button', id: 'shelve'+i, value: 'shelve', sourcePath: sourcePath});
                var histButton  = $('<input />', {type: 'button', id: 'history'+i, value: 'history'});
                var uuidField  = $('<input />', { type: 'hidden', id: 'uuid'+i, value: uuid, class: 'uuidClass'});
                var buildingField  = $('<input />', { type: 'hidden', id: 'building'+i, value: building});
                var measurementField  = $('<input />', { type: 'hidden', id: 'measurement'+i, value: measurementTypeToPrism});
                td = $('<td/>').append(
                    ackButton.prop('outerHTML') + ' ' +
                    shelveButton.prop('outerHTML') + ' ' +
                    // histButton.prop('outerHTML') + ' ' +
                    uuidField.prop('outerHTML') + ' ' +
                    buildingField.prop('outerHTML') + ' ' +
                    measurementField.prop('outerHTML')
                ); 
                row.append(td);
                $('#table1 > tbody').append(row);
            }
        }
        $("#table1").trigger("update"); 
        $('input[type="button"][value="acknowledge"]').click(function(){
            var rowId = this.id.substring(11, this.id.length); 
            var uuidField = $('#uuid'+rowId);
            $('#submitUUID').val(uuidField.val());
            $('#userField').val('');
            $('#notesField').val('');
            $("#dialog").dialog({title: "Acknowledgement"})
            $("#dialog").dialog( "open" );
        });
        $('input[type="button"][value="shelve"]').click(function(){
            var rowId = this.id.substring("shelve".length, this.id.length); 
            $('#rowId').val(rowId);
            $("#dialog_shelving").dialog({title: "Time"})
            $("#dialog_shelving").dialog( "open" );
        });
        $('input[type="button"][value="history"]').click(function(e){ 
            $("#statusButton").show();
            $("#historyButton").hide();
            var rowId = this.id.substring('history'.length, this.id.length); 
            var source = $("#source"+rowId).text(); 
            var sourcePieces = source.split(' ');
            var filterFor = sourcePieces[0];
            $("#title").text(filterFor + " Alarm History").css("color", "gray");;
            $("#subtitle").text('').css("color", "gray");
            setUpHistoryHeader();
            pullHistory();
            clearTimeout(timerPullHistory);
            timerPullHistory = setInterval(pullHistory, 5000);
        }); 
        $("#submitButton").click(function( event ) {
            event.preventDefault();
            var uuid = $('#submitUUID').val();
            var user = $('#userField').val();
            var $form = $( this ),
                notes = $('#notesField').val();
            var posting = $.post(ignitionAlarmsEndPoint, { uuid: uuid, user: user, notes: notes }, function( data ) {
                console.log( "post success" );
                var content = $( data ).find( "#content" );
                $( "#result" ).empty().append( content );
            })
            .done(function(data) {
                console.log('post done: ' + data);
            })
            .fail(function() {
                console.log( "post fail" );
            })
            .always(function() {
                console.log( "post always" );
            }); 
            $( "#dialog" ).dialog( "close" );
            return false;
        });   
        $("#submit_shelving").click(function(event) {
            let rowId = $("#rowId")[0].value; 
            var source_path = $($('#shelve'+rowId)[0]).attr("sourcepath");
            var shelve_time = $("#shelve_time option:selected").val();
            var posting = $.post(ignitionShelveEndPoint, {source_path: source_path, shelve_time: shelve_time}, function( data ) {
                console.log( "post success" );
                var content = $( data ).find( "#content" );
                $( "#result" ).empty().append( content );
            })
            .done(function(data) {
                console.log('post done: ' + data);
            })
            .fail(function() {
                console.log( "post fail" );
            })
            .always(function() {
                console.log( "post always" );
            }); 
            $( "#dialog_shelving" ).dialog( "close" );
            return false;
        });     
    }); 
}
function alarmStatusText(){ 
    $("#dialog").dialog({autoOpen: false, closeOnEscape: false}); 
    $("#dialog_shelving").dialog({autoOpen: false, closeOnEscape: false}); 
    setUpAlarmHeader(); 
    pullAlarmsText(); 
    clearTimeout(timerPullAlarmsText); 
    timerPullAlarmsText = setInterval(pullAlarmsText, 5000); 
} 
$(document).ready(function() { 
    debugger // index22.html
    var latlngc = new google.maps.LatLng(30.2845, -97.735);
    var mapOptions = {
        gestureHandling: 'greedy', 
        zoom: initialZoom,
        center: latlngc,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [
          {
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#f5f5f5"
              }
            ]
          },
          {
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "elementType": "labels.text",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#f5f5f5"
              }
            ]
          },
          {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#bdbdbd"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#eeeeee"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#e5e5e5"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#ffffff"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#dadada"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#e5e5e5"
              }
            ]
          },
          {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#eeeeee"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#c9c9c9"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          }
        ]        
    }; 
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    map.addListener('zoom_changed', function() {
        console.log('zoom: ' + map.getZoom()); 
        showLabels();
    });
    alarmStatusText();
	alarmStatusMap();
    alarmHistory(); 
});