let audio = new Audio("https://www.freesound.org/data/previews/178/178032_1449999-lq.mp3"); 
var map;
let zoom_limit = 16; 
var refresh_interval; // seconds
switch(environment) {
  case "development":
	refresh_interval = 3 * 1000; // seconds
    break;
  case "staging":
	refresh_interval = 30 * 1000; // seconds
    break;
  case "production":
	refresh_interval = 30 * 1000; // seconds
    break;
  default:
    console.log("no environment set");
}
var time_filtered = false;
let initial_zoom = 17; 
var label_switch_state = false;
var initial_center = new google.maps.LatLng(30.2915, -97.7406);
var initial_center = new google.maps.LatLng(30.28485108647737, -97.73391849507796);
var timer_alarms_continuously; 
var dont_care_after = 5 * 60 * 1000; // (milliseconds)
var markers = [];
var building_labels = [];
var showMarkers = {};
var stateChangedAt = {};
var stateChangedPreviouslyAt = {};
var icon_scale_old;
var buildingAlarmStates = {};
var user_name = "";
var password = "";
var tag_limit = 500 - 1; 
var acquisition_type_filter_array = [];
var tags;
var plcs_in_building;
var dialog_login;
var dialog_date_range;
var column_to_sort = "time";
var sort_ascending = true; 
var password;
var first_time = true;
var filter_parameter_array = [];

function show_tags_page(){
	$("#tag_table").css("display","block");
	$("#tag_history_region").css("display","none");
	$("#multitrend_region").css("display","none");
}
function tag_history(){
	$("#tag_table").css("display","none");
	$("#tag_history_region").css("display","block");
	$("#multitrend_region").css("display","none");

	var checkBoxes = $('input[type="checkbox"]:checked');
	var tag_needing_history;
    checkBoxes.each(function(index){
    	tag_needing_history = $(this).parent().parent().attr("who").split(":/")[1].split(":")[1];
    });
	checkBoxes.each(function(index){
	  $(this).prop('checked', false);
	});
	var tag_history_for = $("#tag_history_for");
	tag_history_for.empty();
	var plc       = tag_needing_history.split('/')[0].toUpperCase() + " ";
	var building  = tag_needing_history.split('/')[1].toUpperCase() + " ";
	var bma_count = tag_needing_history.split('/')[2].split('_').length;
	var acquisition_type = "";
	var meter;
	switch(bma_count){
		case 2:
			meter = tag_needing_history.split('/')[2].toUpperCase() + " ";
			break;
		case 3:
			meter = tag_needing_history.split('/')[2].split('_')[1].toUpperCase() + " ";
			acquisition_type = tag_needing_history.split('/')[2].split('_')[2].toUpperCase() + " ";
			break;
	}
	var text = building;
	text = text + plc + "   ";
	text = text + meter + "   ";
	text = text + acquisition_type;
	tag_history_for.text(text);
	$.get(alarm_service + '/tag_history?tag_needing_history=' + tag_needing_history, function(data, status){
		var tags = JSON.parse(data);

		var tag_history_body = $("#tag_history_body");
		tag_history_body.empty();
		for(tag of tags){
			var row = $("<div>");
			row.addClass("tag_history_row");
			var when = $('<div>').text(new Date(tag.when_alarmed).toLocaleString());
			when.addClass("tag_history_column");
			when.addClass("tag_history_column0");
			row.append(when);

			var became = $('<div>').text(tag.became); 
			if(became.text().indexOf('Cleared') != -1){
				became.addClass('pill_green');
			} else if(became.text().indexOf('Acknowledged') != -1){
				became.addClass('pill_yellow');
			} else {
				became.addClass('pill_red');
			}
			became.addClass("tag_history_column_pill");
			row.append(became);

			if(tag.became.toLowerCase().indexOf("active") != -1){
				var because_details = $('<div>').text(tag.alarmed_data); 
				row.append(because_details);
			} else {
				var because_details = $('<div>').text(tag.cleared_data); 
			}
			because_details.addClass("tag_history_column");
			row.append(because_details);

			tag_history_body.append(row);
		}
	});	

    var openDropdown = document.getElementById("dropdown-content");
	openDropdown.classList.toggle('show');
}

function time_convertor(date_time) {
    var PM = date_time.match('PM') ? true : false;
    var date_portion = date_time.split(",")[0];
    var time_portion = date_time.split(",")[1];
    var hour = time_portion.split(':')[0];
    var min = time_portion.split(':')[1];
    var sec = time_portion.split(':')[2];
    
    if (PM) {
        var hour = 12 + parseInt(hour,10);
        var sec = sec.replace('PM', '');
    } else {
        var sec = sec.replace('AM', '');
        var hour = hour.replace('12', '00');
    }
    
    return date_portion + " " + hour + ':' + min + ':' + sec;
}

function alarms_for_management_table(column_to_sort, sort_ascending){
	$.get(alarm_service + '/alarms_for_management_table?column_to_sort=' + column_to_sort + '&sort_ascending=' + sort_ascending, function(data, status){
		if(data.indexOf("malformed auth") == -1){
	        $("#user_name_div").text(user_name);
			var data = JSON.parse(data);
			if(data.ent_opc_connection_state.toLowerCase().indexOf('connect') != -1){
				var tags = data.tags; 
				tags_for_table(tags);
				// filter_table();
				calculate_highlights();
				if(first_time){
					sort_column('Time');
				}
				var updated_at_time_stamp = new Date();
				var year = updated_at_time_stamp.getFullYear();
				var month = ("0" + (updated_at_time_stamp.getMonth() + 1)).slice(-2);
				var day = ("0" + updated_at_time_stamp.getDate()).slice(-2);
				var hour = ("0" + updated_at_time_stamp.getHours()).slice(-2);
				var minute = ("0" + updated_at_time_stamp.getMinutes()).slice(-2);
				var second = ("0" + updated_at_time_stamp.getSeconds()).slice(-2);
				var updated_at_text = month + "/" + day + "/" + year + " " + hour + ":" + minute + ":" + second;
				$('#updated_at').text(updated_at_text);
			} else {
				var table_body = $("#table_body");
				table_body.empty();
				table_body.html("<h1>ENT_OPC not connected<h1>");

			}

		} else {
	        $("#user_name_div").text("unknown");
			alert("malformed authorization");
		}
	}).fail(function(){
		var table_body = $("#table_body");
		table_body.empty();
		table_body.html("<h1>IA not connected<h1>");
	});
}

function calculate_highlights(){
	if($("#tag_table").css("display") == "block"){
	  var alarming_unacknowledged = 0;
	  var alarming_acknowledged = 0;
	  var cleared_unacknowledged = 0;
	  var records = $('#table_body > div');
	  records.each(function(index, record){
	    if($(record).is(':visible')){
	    	try{
				var alarming = JSON.parse($($($(record).children().get(8)).children()[1]).val());
				var acknowledged = JSON.parse($($(record).children().get(9)).attr("value"));
				alarming_unacknowledged = alarming_unacknowledged + ( alarming && !acknowledged ? 1 : 0);
				alarming_acknowledged   = alarming_acknowledged   + ( alarming &&  acknowledged ? 1 : 0);
				cleared_unacknowledged  = cleared_unacknowledged  + (!alarming && !acknowledged ? 1 : 0);
	    	} catch(error) {
	    		console.log("Error in calculate_highlights: " + error + ". Data: " + JSON.stringify(record))
	    	}
	    }
	  });
	  $("#alarming_unacknowledged").text(alarming_unacknowledged);
	  $("#alarming_acknowledged").text(alarming_acknowledged);
	  $("#cleared_unacknowledged").text(cleared_unacknowledged);
	}
}

function isInfoWindowOpen(infoWindow){
    var map = infoWindow.getMap();
    return (map !== null && typeof map !== "undefined");
}

function tags_for_table(tags){
  // var thead = $('#table_header'); // .empty();
  var tbody = $('#table_body'); // .empty(); 
  if(tags.length == 0){
    // tbody.append($("<tr><td><span >No alarms</span></td></tr>")); 
  } else {
	    var should_siren = false;
	    for(tag of tags){ 
	      should_siren = should_siren || tag.should_siren;
		  var new_row = $('<div></div>');
	      new_row.addClass("Rtable");
		  new_row.attr("who", tag.who);
		  new_row.attr("row_id", tag.uuid);
	      var column = $('<div class="column_checkbox"></div>').append($('<input />', {type: 'checkbox'}));
	      column.addClass("column0");
	      new_row.append(column);
	      if(tag.cleared == tag.alarming){
	        console.log("oh oh!");
	      }
	      var event_time = tag.active_data.time;
	      var state = 2*tag.acknowledged + tag.alarming;
	      switch(state) {
	        case 0:
	          pill_color = 'pill_blue';
	          break;
	        case 1:
	          pill_color = 'pill_red';
	          break;
	        case 2:
	          pill_color = 'pill_yellow';
	          try{
	          	user  = tag.acknowledged_data.user  ? tag.acknowledged_data.user.split(':')[1] : "(na)";
	          	notes = tag.acknowledged_data.notes ? tag.acknowledged_data.notes              : "(na)";
	          } catch(err) {
	          	console.log(err);
	          	user  = "(error)";
	          	notes = "(error)";
	          }
	          break;
	        case 3:
	          pill_color = 'pill_green';
	          try{
	            user = tag.acknowledged_data.user.split(':')[1];
	            notes = tag.acknowledged_data.notes;
	          } catch(err) {
	            user = "(error)";
	            notes = "(error)";
	          }
	          break;
	        default:
	          console.log("oh oh!");
	      }
	      var column = $('<div></div>');
	      column.addClass("Rtable-cell Rtable-cell1 column1");
	      var event_time_object = new Date(event_time);
	      var year = event_time_object.getFullYear();
	      year = '' + year;
	      year.substr(2, year.length);
	      year = year.substr(year.length -2)
	      var month = event_time_object.getMonth() + 1;
	      if(month < 10){
	      	month = '0' + month;
	      }
	      var day = event_time_object.getDate();
	      if(day < 10){
	      	day = '0' + day;
	      }
	      var hour = event_time_object.getHours();
	      if(hour < 10){
	      	hour = '0' + hour;
	      }
	      var minute = event_time_object.getMinutes();
	      if(minute < 10){
	      	minute = '0' + minute;
	      }
	      var seconds = event_time_object.getSeconds();
	      if(seconds < 10){
	      	seconds = '0' + seconds;
	      }

	      column = column.text(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + seconds);
	      column.attr("column","time");
	      var hidden_timestamp = $('<input>');
	      hidden_timestamp.attr("type", "hidden");
	      hidden_timestamp.val(tag.active_data.time);
	      var column = column.append(hidden_timestamp);
	      column.addClass("column0 column_time");
	      new_row.append(column);
	      try{
	        var source = tag.who.split(':/')[0].split(':')[1];
	        var path = tag.who.split(':/')[1].split(':')[1];
	        var alarm_name = tag.who.split(':/')[2].split(':')[1];

	        var num_path_pieces = path.split('/').length;
	        if(num_path_pieces == 2){
	          var building = path.split('/')[0].toUpperCase();
	          var meter    = path.split('/')[1].toUpperCase();
	        } else {
	          var building = (path.split('/')[0] + '.' + path.split('/')[1]).toUpperCase();
	        }
	      } catch(err) {
	        var tag_path = tag.who + ' (where is?)';
	      }
	      var column = $('<div></div>');
	      column.attr("column", "building");
	      try{
	        column.attr("lat", tag.where.lat);
	        column.attr("lng", tag.where.lng);
	      } catch(err){
	        // console.log('no location for ' + tag.who);
	      }
	      column.text(tag.building.toUpperCase());
	      column.addClass("column0");
	      new_row.append(column);

	      try{
		      var column = $('<div column="plc"></div>').text(tag.plc.toUpperCase());
		      column.addClass("column0");
		      new_row.append(column);

	      } catch(error) {
	      	console.log("Error displaying plc: " + error + ". Data: " + JSON.stringify(tag))
	      }

	      var column = $('<div column="meter"></div>').text(tag.meter);
	      column.addClass("column0");
	      new_row.append(column);

	      // if(tag.acquisition_type == "PD") { // remove when not turning off PD's
	      //   continue;
	      // }
	      var column = $('<div column="meter_type"></div>').text(tag.acquisition_type);
	      column.addClass("column0");
	      new_row.append(column);

	      var setpointA;
	      if("setpointA" in tag.active_data){
	        try{
	          setpointA = parseInt(JSON.parse(tag.active_data.setpointA));
	        } catch (error) {
	          setpointA = tag.active_data.setpointA;
	        }
	      } else {
	        setpointA = "(no setpoint)";
	      }
	      var mode;
	      if("mode" in tag.active_data){
	        mode = tag.active_data.mode;
	      } else {
	        mode = "=";
	      }
	      var eventValue;
	      try{
	        eventValue = JSON.parse(tag.active_data.eventValue);
	        eventValue = Math.trunc(eventValue);
	      } catch (error) {
	        eventValue = tag.active_data.eventValue;
	      }
	      var mode;
	      if("mode" in tag.active_data){
	        mode = tag.active_data.mode;
	      } else {
	        mode = "=";
	      }
	      switch(mode){
	        case "Below Setpoint":
	          mode = "<";
	          break;
	        case "Above Setpoint":
	          mode = ">";
	          break;
	        case "Not Equal":
	          mode = "Not";
	          break;
	        default:
	      }
	      if("setpointA" in tag.active_data){
	        var column = $('<div column="condition"></div>').text(alarm_name + ' ' + mode + ' ' + setpointA);
	      } else {
	        var column = $('<div column="condition"></div>').text(alarm_name);
	      }
	      column.addClass("column0");
	      new_row.append(column);
	      var column = $('<div column="event_value"></div>').text(eventValue);
	      column.addClass("column0");
	      new_row.append(column);

	      var column = $('<div column="alarming"></div>');
	      column.addClass('Rtable-cell Rtable-cell8 column8 dot ' + pill_color);
	      var hidden_alarm_color = $('<input>');
	      hidden_alarm_color.attr("type", "hidden");
	      hidden_alarm_color.attr("value", pill_color);
	      column.append(hidden_alarm_color)
	      var hidden_alarm_state = $('<input>');
	      hidden_alarm_state.attr("type", "hidden");
	      hidden_alarm_state.attr("value", tag.alarming);
	      column.append(hidden_alarm_state);
	      column.addClass("column0");
	      new_row.append(column);

	      if(tag.acknowledged){
	        var column = $('<div></div>').css('text-align','left').text(user + ': ' + notes + ' at ' + new Date(tag.acknowledged_data.time).toLocaleString()); 
	      	column.addClass("Rtable-cell Rtable-cell9 column9");
	      	column.css("font-size","60%");
	      	column.attr("column","acknowledge");
	      } else {
	        var column = $('<div></div>').addClass("acknowledge_region").attr("column","acknowledge").css('text-align','left').append('press to acknowledge');
	      	column.addClass("Rtable-cell Rtable-cell9 column9 Rtable-cell_empty");
	      	column.css("font-size","16px");
	      }
	      column.attr("value", tag.acknowledged);
	      column.addClass("column0");
	      new_row.append(column);

	      var column = $('<div ></div>');
	      column.addClass("column0 column_hamburger");
	      new_row.append(column);

	    	var old_row = $("[who='"+tag.who+"']");
	    	if(old_row.length == 0){ // new row
		        if(tbody.children().length < tag_limit) 
		          tbody.prepend(new_row);
	    	} else {
		        var previous_checkbox_state = $(old_row.find('input')[0]).is(':checked');
		        var previous_visibility     = old_row.css('display');
		        new_row.find('input').prop('checked', previous_checkbox_state);
		        new_row.css('display', previous_visibility);
	    		old_row.replaceWith(new_row);
	    	}
	      try{
	        var exclude_from_prism = tag.plc.toLowerCase().indexOf("_kw") != -1;
	        exclude_from_prism = exclude_from_prism || tag.acquisition_type.toLowerCase().indexOf("error") != -1;
	        exclude_from_prism = exclude_from_prism || tag.acquisition_type.toLowerCase().indexOf("run") != -1;
	        exclude_from_prism = exclude_from_prism || tag.acquisition_type.toLowerCase().indexOf("sweeptime") != -1;
	        if(exclude_from_prism){
	          var checkbox = $($($($(new_row[0]).children())[0]).children()[0]);
	          checkbox.attr("disabled", true);
	        }
	      } catch(err) {
	        console.log('try error occurred. but, maybe ok.')
	      }
	    } 
	    for(tag of tags){
			if(tag.should_siren){
				var in_development = false;
				var in_development = in_development || alarm_service.indexOf("http://192.168.56.101:8080") == -1;
				var in_development = in_development || alarm_service.indexOf("http://10.101.206.106:8080") == -1;
				if(!in_development){
					show_popup(tag, false);
				}
		    }
	    }
	    $("#table_body > div").off("click");
	    $("#table_body > div").click(function(event) {
	      if($(event.target).is("div")){
	        event.preventDefault();
	        if($(event.target).attr("column") == "acknowledge"){
	        	if($(event.target).text().toLowerCase().includes("press")){
		          var row_id = $(this).attr("row_id"); 
		          $('#submitUUID').val(row_id);
		          $('#userField').val($('#user_name_div').text());
		          $('#notesField').val('');
		          $("#dialog_building_alarm_acknowledge").dialog("open");
		          var who_array = $("[row_id="+row_id+"]").attr("who").split(":/")[1].split(":")[1].split("/");
		          var dialog_title = who_array[0] + ' ' + who_array[1] + ' ' + who_array[2];
		          $("#dialog_building_alarm_acknowledge").parent().find("span.ui-dialog-title").html(dialog_title);
	        	}
	        } else {
	          var row_id = $(this).attr("row_id");
	          var found_tag;
	          for(tag of tags){
	          	if(tag.uuid == row_id){
	          		found_tag = tag;
	          		break;
	          	}
	          };
	          show_popup(tag, true);
	        }
	      } else if($(event.target).is("input")){
	        // $(event.target).val("true");
	      }
	      return true;
	    }); 
	}
	if(should_siren){
		audio.play();
	} else {
		audio.pause();
	}
	var rows = $("#table_body").children();
	rows.each(function(index){
		var row = $(this);
		var uuid = row.attr("row_id");
		for(tag of tags){
			var found = tag.uuid == uuid;
			if(found)
				break;
		}
		if(!found){
			row.remove();
		}
	});
}

function show_popup(tag, human){
  if(typeof tag.where != "undefined"){
    google.maps.event.trigger(map, 'resize');
    var lat = tag.where.lat;
    var lng = tag.where.lng;
    map.setCenter({lat:lat, lng:lng});
    var focused_marker_icon = {
        path: 'M 36,16 C 36,9.37 30.63,4 24,4 17.37,4 12,9.37 12,16 c 0,9 12,22 12,22 0,0 12,-13 12,-22 z m -16,0 c 0,-2.21 1.79,-4 4,-4 2.21,0 4,1.79 4,4 0,2.21 -1.79,4 -4,4 -2.21,0 -4,-1.79 -4,-4 z',
        fillColor: '#000000',
        fillOpacity: 1,
        scale: 200,
    };
    var marker = markers.find(function(marker){
      return marker.title.toUpperCase() == tag.building.toUpperCase();
    })
    // marker.setIcon({
    //   anchor: new google.maps.Point(15, 10),
    //   path: 'M 36,16 C 36,9.37 30.63,4 24,4 17.37,4 12,9.37 12,16 c 0,9 12,22 12,22 0,0 12,-13 12,-22 z m -16,0 c 0,-2.21 1.79,-4 4,-4 2.21,0 4,1.79 4,4 0,2.21 -1.79,4 -4,4 -2.21,0 -4,-1.79 -4,-4 z',
    //   scale: 0.5,
    //   fillColor: marker.icon.fillColor,
    //   fillOpacity: marker.icon.fillOpacity
    // });
    marker.info_window = build_info_window(tag.building);
    if(!isInfoWindowOpen(marker.info_window)){
      marker.info_window.open(map, marker);
      console.log("wow")
    }
  } else {
  	if(human){
    	alert("building has no location");
  	}
  }
}

function build_info_window(building){
  var row_data = find_all_meters_for_building(building);
  var content_string = "";
  content_string = "<h3>" + building.toUpperCase() + "</h3>";
  for(row_datum of row_data){
    content_string = content_string + "<p>" + row_datum.plc + " " + row_datum.meter + " " + row_datum.meter_type + " " + row_datum.condition + " " + row_datum.event_value  + "</p>";
  }
  var info_window = new google.maps.InfoWindow({
    content: content_string
  });
  google.maps.event.addListener(info_window,'closeclick',function(){
    var building = get_building_from_info_window(this);
    var marker = get_marker_for_building(building);
    set_marker_icon_to_traingle(marker);
    google.maps.event.clearListeners(map, 'closeclick');
    // set_labels_per_zoom();
  });
  return info_window;
}

function add_marker(tag){
  // create info window
  // find all alarms for this building
  var building = tag.building;

  // create marker
  var dot_plc_marker = {
      path: 'M 614.574,504.94 L 335.167,20.996 C 330.788,13.412 322.696,8.738 313.938,8.738 C 305.179,8.738 297.087,13.412 292.708,20.996 L 13.303,504.94 C 8.925,512.524 8.925,521.87 13.303,529.456 C 17.682,537.04 25.775,541.712 34.533,541.712 L 593.344,541.712 C 602.102,541.712 610.196,537.04 614.574,529.456 C 618.953,521.87 618.951,512.524 614.574,504.94 z',
      fillColor: '#ff00ff'
  };
  var dot_chilled_marker = {
      path: 'M 614.574,504.94 L 335.167,20.996 C 330.788,13.412 322.696,8.738 313.938,8.738 C 305.179,8.738 297.087,13.412 292.708,20.996 L 13.303,504.94 C 8.925,512.524 8.925,521.87 13.303,529.456 C 17.682,537.04 25.775,541.712 34.533,541.712 L 593.344,541.712 C 602.102,541.712 610.196,537.04 614.574,529.456 C 618.953,521.87 618.951,512.524 614.574,504.94 z',
      fillColor: '#3498db'
  };
  var dot_electricity_marker = {
      path: 'M 614.574,504.94 L 335.167,20.996 C 330.788,13.412 322.696,8.738 313.938,8.738 C 305.179,8.738 297.087,13.412 292.708,20.996 L 13.303,504.94 C 8.925,512.524 8.925,521.87 13.303,529.456 C 17.682,537.04 25.775,541.712 34.533,541.712 L 593.344,541.712 C 602.102,541.712 610.196,537.04 614.574,529.456 C 618.953,521.87 618.951,512.524 614.574,504.94 z',
      fillColor: '#f1c40f'
  };
  var dot_steam_marker = {
      path: 'M 614.574,504.94 L 335.167,20.996 C 330.788,13.412 322.696,8.738 313.938,8.738 C 305.179,8.738 297.087,13.412 292.708,20.996 L 13.303,504.94 C 8.925,512.524 8.925,521.87 13.303,529.456 C 17.682,537.04 25.775,541.712 34.533,541.712 L 593.344,541.712 C 602.102,541.712 610.196,537.04 614.574,529.456 C 618.953,521.87 618.951,512.524 614.574,504.94 z',
      fillColor: '#c0392b'
  };
  var dot_water_marker = {
      path: 'M 614.574,504.94 L 335.167,20.996 C 330.788,13.412 322.696,8.738 313.938,8.738 C 305.179,8.738 297.087,13.412 292.708,20.996 L 13.303,504.94 C 8.925,512.524 8.925,521.87 13.303,529.456 C 17.682,537.04 25.775,541.712 34.533,541.712 L 593.344,541.712 C 602.102,541.712 610.196,537.04 614.574,529.456 C 618.953,521.87 618.951,512.524 614.574,504.94 z',
      fillColor: '#1abc9c'
  };
  var dot_multiple_marker = {
      path: 'M 614.574,504.94 L 335.167,20.996 C 330.788,13.412 322.696,8.738 313.938,8.738 C 305.179,8.738 297.087,13.412 292.708,20.996 L 13.303,504.94 C 8.925,512.524 8.925,521.87 13.303,529.456 C 17.682,537.04 25.775,541.712 34.533,541.712 L 593.344,541.712 C 602.102,541.712 610.196,537.04 614.574,529.456 C 618.953,521.87 618.951,512.524 614.574,504.94 z',
      fillColor: "#a200ff"
  };
  var dot_unknown_marker = {
      path: 'M 614.574,504.94 L 335.167,20.996 C 330.788,13.412 322.696,8.738 313.938,8.738 C 305.179,8.738 297.087,13.412 292.708,20.996 L 13.303,504.94 C 8.925,512.524 8.925,521.87 13.303,529.456 C 17.682,537.04 25.775,541.712 34.533,541.712 L 593.344,541.712 C 602.102,541.712 610.196,537.04 614.574,529.456 C 618.953,521.87 618.951,512.524 614.574,504.94 z',
      fillColor: '#000000'
  };
  switch(tag.utility_type){
    case "plc":
      var icon = dot_plc_marker; 
      break;
    case "chilled water":
      var icon = dot_chilled_marker; 
      break;
    case "electricity":
      var icon = dot_electricity_marker; 
      break;
    case "steam":
      var icon = dot_steam_marker; 
      break;
    case "water":
      var icon = dot_water_marker; 
      break;
    default:
      var icon = dot_unknown_marker; 
      // console.log(tag.who);
  }
	if(tag.multiple_alarms){
		var icon = dot_multiple_marker; 
	}
	marker = new google.maps.Marker({
		title: tag.building.toUpperCase(),
		position: tag.where,
		icon: {
			path: google.maps.SymbolPath.CIRCLE,
			scale: 4,
			fillColor: icon.fillColor,
			fillOpacity: 1,
			strokeColor: icon.fillColor
		},
		draggable: true,
		map: map
	});	

	marker.addListener('click', function() {
		show_popup(tag, true);
	});
	marker.addListener('mouseover', function() {
		var icon = this.icon;
		icon_scale_old = icon.scale;
		icon.scale = 5;
		this.setIcon(icon);
	});
	marker.addListener('mouseout', function() {
		var icon = this.icon;
		icon.scale = icon_scale_old;
		this.setIcon(icon);
	});
	markers.push(marker); 

	if("where" in tag){
	    var off_set = 0.00005 * initial_zoom;
	    var zoom_level = map.getZoom();
	    var lat = tag.where.lat + off_set / zoom_level*2;
	    var lng = tag.where.lng + off_set / zoom_level*2;
		var building_location = new google.maps.LatLng({lat: lat, lng: lng});
		label_style = "building-name-style-close";
	    var building_label = new TxtOverlay(building_location, tag.building.toUpperCase(), label_style, map);
	    building_labels.push(building_label); 
	} 
  // set_labels_per_zoom();
} 

function turn_labels_off(){
  building_labels.forEach(function(building_label){
    building_label.setMap(null)
  })
}

function turn_labels_on(){
  building_labels.forEach(function(building_label){
    building_label.setMap(map)
  })
}

function get_building_from_marker(marker){
  return marker.title.toUpperCase();
}

function get_tag_for_building(building){
  for(tag of tags){
    if(tag.building.toUpperCase() == building.toUpperCase()){
      return tag;
    }
  }
}
function set_marker_icon_to_traingle(marker){
  var building = get_building_from_marker(marker);
  remove_from_map(marker);
  var tag = get_tag_for_building(building);
  add_marker(tag);
}

function get_marker_for_building(building){
  for(marker of markers){
    if(marker.title.toUpperCase() == building.toUpperCase()){
      return marker;
    };
  }
  return "null";
}

function get_building_from_info_window(info_window){
  var building = $(info_window.getContent()).first().text();
  return building;
}

function filter_table(from, to){
  var tr, i;
  var rows = $("#table_body > div");
  rows.each(function(index){ 
    tr = $(this);
    var display_row = true;
    var row_contents = [];
    for(i=1; i != 6; i++){
      var row_field = tr.children().get(i);
      if($(row_field).children().length == 0){
        row_contents[i] = $(row_field).text().toUpperCase();
      } else {
        row_contents[i] = $(row_field).children().first().val();
      }
    }
    var filter_values = [];
    for(i = 2; i != 5; i++){
      filter_values[i] = $($(".filter_input")[i]).val().toUpperCase();
      display_row = (row_contents[i].indexOf(filter_values[i].toUpperCase()) != -1) && display_row;
    }
    var matched_aquisition_type;
    if(acquisition_type_filter_array.length == 0){
      matched_aquisition_type = true;
    } else {
      matched_aquisition_type = acquisition_type_filter_array.indexOf(row_contents[5].toUpperCase()) != -1; 
    }
    display_row = matched_aquisition_type && display_row;
    var matched_date_range = true;
    if($("#from").val() != ""){
		var from = (new Date($("#from").val())).getTime();
		var to   = (new Date($("#to").val())).getTime();
    	var row_time_stamp = Number(row_contents[1]);
    	if(from <= row_time_stamp && row_time_stamp <= to){
    		matched_date_range = true;
    	} else {
    		matched_date_range = false;
    	}
    }
    display_row = matched_date_range && display_row;
    if(display_row){
      $(this).css("display","")
    } else {
      $(this).css("display","none")
    }
  });
  calculate_highlights();
  alarms_for_management_map();
}

function find_all_meters_for_building(building){
  var rows = $("#table_body > div:visible");
  var row_data = [];
  for(index = 0; index != rows.length; index++){
    var row = rows.get(index);
    var building_in_row = $(row).find("[column='building']").text();
    if(building.toUpperCase() == building_in_row.toUpperCase()){
      var plc = $(row).find("[column='plc']").text();
      var meter = $(row).find("[column='meter']").text();
      var meter_type = $(row).find("[column='meter_type']").text();
      var condition = $(row).find("[column='condition']").text();
      var event_value = $(row).find("[column='event_value']").text();
      var row_datum = {
        "plc":plc, 
        "meter":meter, 
        "meter_type":meter_type, 
        "condition":condition, 
        "event_value":event_value,
      };
      row_data.push(row_datum);
    }
  }
  return row_data;
}

function remove_markers_not_sent(tags){
  for(marker of markers){
    var found = false;
    for(tag of tags){
      if(marker.title.toUpperCase() == tag.building.toUpperCase()){
        found = true;
      }
    }
    if(!found){
      marker.setMap(null); 
      var filtered = markers.filter(function(value, index, arr){
        return value.title != marker.title;
      });
      markers = filtered;
      building_labels.forEach(function(building_label){
        var building_label_to_remove = tag.who.split('/')[1].split(':')[1].toUpperCase();
        if(building_label.txt_ == building_label_to_remove){
          building_label.setMap(null);
          var filtered = building_labels.filter(function(value, index, arr){
            return value.txt_ != building_label_to_remove;
          }); 
          building_labels = filtered;
        }
      });
    }
  };
}

function is_on_map(tag){ 
  var found = false;
  for(marker of markers){
    if(tag.building.toUpperCase() == marker.title.toUpperCase()){
      return true;
    }
  }
}

function is_in_table(tag){
  var rows = $("#table_body > div");
  for(index = 0; index != rows.length; index++){
    var row = rows.get(index);
    if(tag.building.toUpperCase() == $(row).find("[column='building']").text().toUpperCase()){
      if($(row).css("display") != "none"){
        return true;
      }
    }
  }
  return false;
}

function remove_from_map(marker){
  marker.setMap(null); 
  var filtered = markers.filter(function(value, index, arr){
    return value.title != marker.title;
  });
  markers = filtered;
  building_labels.forEach(function(building_label){
    var building_label_to_remove = tag.building.toUpperCase();
    if(building_label.txt_ == building_label_to_remove){
      building_label.setMap(null);
      var filtered = building_labels.filter(function(value, index, arr){
        return value.txt_ != building_label_to_remove;
      }); 
      building_labels = filtered;
    }
  });
}

function find_marker_for(tag){
  for(marker of markers){
    if(tag.building.toUpperCase() == marker.title.toUpperCase()){
      return marker;
    }
  }
  return "null";
}
function remove_markers_filtered_against_by(tags){
  for(tag of tags){
    var keep = is_in_table(tag);
    if(!keep){
      var marker = find_marker_for(tag);
      if(marker != "null"){
        remove_from_map(marker);
      }
    }
  }
}

function add_markers(tags){
  for(tag of tags){
    var on_map = is_on_map(tag);
    if(!on_map){
      var in_table = is_in_table(tag);
      if(in_table){
        add_marker(tag);
      }
    }
  } 
}

function alarms_for_management_map(){
  $.get(alarm_service + '/alarms_for_management_map', function(body, status){
    tags = JSON.parse(body); 
    remove_markers_not_sent(tags);
    remove_markers_filtered_against_by(tags);
    add_markers(tags);
	if($("#label_switch").find("input").prop("checked")){
		turn_labels_on();
	} else {
		turn_labels_off();
	}
  }); 
}

function report_problem_with_kw_opc(){
  var row = $("<tr></tr>");
  var column = $("<td><h1>ENT OPC Error</h1></td>");
  row.append(column); 
  var tbody = $('#table_body').empty();
  tbody.append(row);
  $("#alarming_unacknowledged").text("Error");
  $("#alarming_acknowledged").text("Error");
  $("#cleared_unacknowledged").text("Error");
}

function ent_opc_check(){
  $.get(alarm_service + '/alarms_for_management_table?column_to_sort=ent_check', function(body, status){
    var data = JSON.parse(body); 
    var ent_opc_connection_state = data.ent_opc_connection_state;
    // if(ent_opc_connection_state.indexOf("CONNECTED") == -1){
		  // report_problem_with_kw_opc();
    // } else {
        var column_to_sort = $('#column_to_sort_object').val();
  		alarms_for_management_table(column_to_sort, sort_ascending); 
  		alarms_for_management_map();
		first_time = false;
    // }
  }); 
}

function report_problem_with_ia_opc(){
  var row = $("<tr></tr>");
  var column = $("<td><h1>Not communicating with KWOPC</h1></td>");
  row.append(column); 
  var tbody = $('#table_body').empty();
  tbody.append(row);
}

function ia_opc_check(){
	try{
		$.get(alarm_service + '/alarms_for_management_table?column_to_sort=ia_check', function(body, status){
			if(body.indexOf("No response from IA Server") == -1){
				if(body.indexOf("malformed authorization request") == -1){
					var data = JSON.parse(body); 
					var ia_opc_server_state = data.ia_opc_server_state;
					if(ia_opc_server_state.indexOf("CONNECTED") == -1){
					  report_problem_with_ia_opc();
					} else {
					  ent_opc_check();
					}
				} else {
					alert("Login Failed");
				}
			} else {
			  var row = $("<tr></tr>");
			  var column = $("<td><h1>IA OPC Error</h1></td>");
			  row.append(column); 
			  var tbody = $('#table_body').empty();
			  tbody.append(row);
			  $("#alarming_unacknowledged").text("Error");
			  $("#alarming_acknowledged").text("Error");
			  $("#cleared_unacknowledged").text("Error");
			}
		})
		.fail(function() {
			report_problem_with_ia_opc();
		})
	} catch(err) { 
        console.log('callback try/catch: ' + err);
    }
} 

function run_loop(){
  var sorting = $('#sorting_object').val();
  if(!sorting){
  	console.log("polling");
  	// ia_opc_check();
	var column_to_sort = $('#column_to_sort_object').val();
	alarms_for_management_table(column_to_sort, sort_ascending); 
	alarms_for_management_map();
	first_time = false;
  } else {
  	console.log("sorting (not polling)");
  }
} 

var busyi;
function sort_column(column_to_sort){
  $('#sorting_object').val(true);
  $('#column_to_sort_object').val(column_to_sort);
  sort_ascending = !sort_ascending;
  busyi = new busy_indicator(document.getElementById("busybox"), document.querySelector("#busybox div"));
  busyi.show();
  setTimeout(execute_sort, 1000);
}

function get_filter_parameters(){
	filter_parameter_array = [];
	var filter_gui_elements = $("[id^=filter]");
	filter_gui_elements.each(function(){
		filter_parameter_array.push($(this).val());
	});
	filter_parameter_array.push(acquisition_type_filter_array);
}

function apply_filter_parameters(){
	var filter_gui_elements = $("[id^=filter]");
	filter_gui_elements.each(function(){
		 $(this).val(filter_parameter_array.shift());
	});
	filter_table();
}

function execute_sort(){
  var column_to_sort = $('#column_to_sort_object').val();
  $('#table_body').empty();
  get_filter_parameters();
  alarms_for_management_table(column_to_sort, sort_ascending);
  $('#sorting_object').val(false);
  busyi.hide();
  setTimeout(apply_filter_parameters, 1000);
}

function export_to_file_user(){
	export_to_file();
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
		var openDropdown = dropdowns[i];
		if (openDropdown.classList.contains('show')) {
			openDropdown.classList.remove('show');
		}
    }
}
$(document).ready(function() {
	$("#trend_dropdown")[0].style.cursor = "pointer";
	$("#history_dropdown")[0].style.cursor = "pointer";
	$("#export")[0].style.cursor = "pointer";
	$("#appsheet")[0].style.cursor = "pointer";
	show_tags_page();
	$("#back_button").click(function(){
		show_tags_page();
	});
	$("#back_button_iframe0").click(function(){
		show_tags_page();
	});
	$('.select2').SumoSelect({ okCancelInMulti: true, selectAll: true, placeholder: 'Any type ...' });
	$('.select2')[0].sumo.selectAll();
	$('.select2').on('change', function(evt, meter_type) {
		// console.log($('.select2')[0].sumo.getSelStr());
	    var selections_made = $('.select2')[0].sumo.getSelStr();
	    update_acquisition_type_filter_array(selections_made);
	});
	function call_prism() {
		event.preventDefault();
		var trending_endpoint;
		switch(environment) {
		  case "development":
			trending_endpoint = 'insight.utilities.utexas.edu';
		    break;
		  case "staging":
			trending_endpoint = 'insight.utilities.utexas.edu';
		    break;
		  case "production":
			trending_endpoint = 'insight.utilities.utexas.edu';
		    break;
		  default:
		    console.log("no environment set");
		}
		var prefix = 'https://' + trending_endpoint + '/Insight/api/Charts/QuickTrend/Line/tags/';
		var checkBoxes = $('input[type="checkbox"]:checked');
		var tags = [];
		checkBoxes.each(function(index){
		  var plc = $(this).parent().parent().attr("who").split(":/")[1].split(":")[1].split("/")[1];
		  if(plc.indexOf('_') != -1){
		  	plc = plc.split('_')[0];
		  }
		  var meter = $(this).parent().parent().attr("who").split(":/")[1].split(":")[1].split("/")[2].split("_").splice(1,3).join("_");
			tags.push(plc+'_'+meter);
		});
		var url = prefix + tags.join(','); // 'ADH_A0001_PD'; 
		// document.getElementById("iframe0").src = url;
		window.open(url, '_blank');
		checkBoxes.each(function(index){
		  $(this).prop('checked', false);
		});
		// $("#tag_table").css("display","none");
		// $("#tag_history_region").css("display","none");
		// $("#multitrend_region").css("display","block");
		return true;
	};
	$("#trend_dropdown").click(function(){
		call_prism();
	});
	$("#history_dropdown").click(function(){
		tag_history();
	});
	$("#export").click(function(){
		export_to_file_user();
	    var openDropdown = document.getElementById("dropdown-content");
		openDropdown.classList.toggle('show');
	});
	$("#appsheet").click(function(){
		get_appsheets();
	});
	$("#hamburger_menu").click(function( event ) {
		document.getElementById("dropdown-content").classList.toggle("show");
	});
	$("#label_switch").click(function(){
		if($(this).find("input").prop("checked")){
			turn_labels_on();
		} else {
			turn_labels_off();
		}
	});
	$(".datepicker").datepicker();
	$("#filter_time").css("background-color","white");
	$("#filter_time").css("color","white");
	$("#filter_time").click(function( event ) {
		// this.classList.toggle("change");
		if(time_filtered){
			time_filtered = false;
			$("#from").val("");
			filter_table()
			$("#filter_time").css("background-color","white");
		} else {
  			$('#sorting_object').val(true);
  			dialog_date_range.dialog("open"); 
		} 
	});
	window.onkeypress = function(e) {
		if (e.which == 13) {
			console.log("key pressed");
			$("button:contains('Login')").click();
		}
	}
  $('#table_body').empty(); 
  $("#dialog_building_alarm_acknowledge").dialog({autoOpen: false, title: "Acknowledge"});
  $("#dialog_building_alarm_acknowledge_submit").click(function( event ) {
    event.preventDefault();
    var uuid = $('#submitUUID').val();
    var user = $('#userField').val();
    var notes = $('#notesField').val();
    var row = $('[row_id="' + uuid + '"]');
    var row_notes = row.find('[column="acknowledge"]');
    row_notes.removeClass("Rtable-cell_empty");
    row_notes.text(user + ' says ' + notes);
    row.addClass('bright_labels');
    var posting = $.post( alarm_service + "/acknowledge", { 'uuid': uuid, 'user': user, 'notes': notes }, function( data ) {
      console.log(JSON.parse(data));
    })
    .done(function(data) {
      console.log('post done: ' + data);
    })
    .fail(function() {
      console.log("post fail");
    });
    $("#dialog_building_alarm_acknowledge").dialog("close");
    return false;
  });
  dialog_login = $("#dialog_login").dialog({
    autoOpen: false,
    height: 400,
    width: 350,
    modal: true,
    buttons: {
      "Login": function(event) {
        event.preventDefault();
        user_name = $("#name").val();
        password = $("#password").val();
		switch(environment) {
		  case "development":
	        user_name = "admin";
	        password = "password";
		    break;
		  case "staging":
	        user_name = "arm";
	        password = "5xSqdeCYMJJVgLgVrSuR";
		    break;
		  case "production":
		    break;
		  default:
		    alert("no environment set");
		    return;
		}
    	var posting = $.post( alarm_service + "/login", { 'user_name': user_name, 'password': password }, function(data) {
          if(data.indexOf('GOTIT')){
            run_loop();
            clearTimeout(timer_alarms_continuously); 
            timer_alarms_continuously = setInterval(run_loop, refresh_interval); 
          } else {
          	alert("credentials failed");
          }
        }).fail(function() {
          alert("unable to communicate with uem metering data");
        });
        dialog_login.dialog("close");
        return false;
      },
      Cancel: function() {
        dialog_login.dialog( "close" );
      }
    },
    close: function() {
    }
  }); 
  dialog_date_range = $("#dialog_date_range").dialog({
    autoOpen: false,
    height: 400,
    width: 350,
    modal: true,
    buttons: {
      "OK": function(event) {
        event.preventDefault();
        filter_table();
		time_filtered = true;
		$("#filter_time").css("background-color","#888888");
        dialog_date_range.dialog("close");
  		$('#sorting_object').val(false);
		// $(".hamburger-container")[0].classList.toggle("change");
        return false;
      },
      Cancel: function() {
        event.preventDefault();
  		$('#sorting_object').val(false);
		time_filtered = false;
        dialog_date_range.dialog( "close" );
		// $(".hamburger-container")[0].classList.toggle("change");
      }
    },
    close: function() {
    }
  });  
	$('body').css("color", "gray");
  var mapOptions = {
      gestureHandling: 'greedy', 
      zoom: initial_zoom,
      streetViewControl: false,
      fullscreenControl: true,
      center: initial_center,
      mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [
		  {
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#f5f5f5"
		      }
		    ]
		  },
		  {
		    "elementType": "labels",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "elementType": "labels.icon",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#616161"
		      }
		    ]
		  },
		  {
		    "elementType": "labels.text.stroke",
		    "stylers": [
		      {
		        "color": "#f5f5f5"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative.land_parcel",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative.land_parcel",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#bdbdbd"
		      }
		    ]
		  },
		  {
		    "featureType": "administrative.neighborhood",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "poi",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "poi",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#eeeeee"
		      }
		    ]
		  },
		  {
		    "featureType": "poi",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#757575"
		      }
		    ]
		  },
		  {
		    "featureType": "poi.park",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#e5e5e5"
		      }
		    ]
		  },
		  {
		    "featureType": "poi.park",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#9e9e9e"
		      }
		    ]
		  },
		  {
		    "featureType": "poi.school",
		    "elementType": "geometry.fill",
		    "stylers": [
		      {
		        "visibility": "on"
		      }
		    ]
		  },
		  {
		    "featureType": "road",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#ffffff"
		      }
		    ]
		  },
		  {
		    "featureType": "road",
		    "elementType": "labels.icon",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "road.arterial",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#757575"
		      }
		    ]
		  },
		  {
		    "featureType": "road.highway",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#dadada"
		      }
		    ]
		  },
		  {
		    "featureType": "road.highway",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#616161"
		      }
		    ]
		  },
		  {
		    "featureType": "road.local",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#9e9e9e"
		      }
		    ]
		  },
		  {
		    "featureType": "transit",
		    "stylers": [
		      {
		        "visibility": "off"
		      }
		    ]
		  },
		  {
		    "featureType": "transit.line",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#e5e5e5"
		      }
		    ]
		  },
		  {
		    "featureType": "transit.station",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#eeeeee"
		      }
		    ]
		  },
		  {
		    "featureType": "water",
		    "elementType": "geometry",
		    "stylers": [
		      {
		        "color": "#c9c9c9"
		      }
		    ]
		  },
		  {
		    "featureType": "water",
		    "elementType": "labels.text.fill",
		    "stylers": [
		      {
		        "color": "#9e9e9e"
		      }
		    ]
		  }
		]
	}; 
  map = new google.maps.Map(document.getElementById("map"), mapOptions);

  $('#user_name').val('');
  $('#password').val('');
  dialog_login.dialog("open"); 

  // $('.select2').SumoSelect({ okCancelInMulti: true, selectAll: true, placeholder: 'Any type ...' });
  // $('.select2')[0].sumo.selectAll();
  // $('.select2').on('change', function(evt, meter_type) {
  //   var selections_made = $('.select2')[0].sumo.getSelStr();
  //   update_acquisition_type_filter_array(selections_made);
  // });
    $( "#legend_box" ).dialog({
      autoOpen: false
    });
	$( "#legend_control" ).click(function() {
		$( "#legend_box" ).dialog( "open" );
		$( "#legend_box" ).position( {my: "left top"} );
	});
}); 

var appsheet_buildings = {
	'GRE-N': "GRE-N: Gregory Gym - North",
	'GRE-S': "GRE-S: Gregory Gym - South",
	'JCD-E': "JCD-E: Jester Center Dormitory (East)",
	'JCD-W': "JCD-W: Jester Center Dormitory (West)",
	'JES-A': "JES-A: Beauford H. Jester Center-Academic",
	'PHR(new)': "PHR(new): Pharmacy Building - new",
	'PHR(old)': "PHR(old): Pharmacy Building - old",
	'RLM_2': "RLM_2: Robert Lee Moore Bldg. - for W2113 Meter",
	'STD-E': "STD-E: Darrell K Royal-Texas Memorial Stadium (East)",
	'GDC-N': "GDC-N: Gates Dell Complex",
	'GDC-S': "GDC-S: Gates Dell Complex",
	'FAC-R': "FAC-R: FAC                      Outside     Rotunda",
	'ARC-New': "ARC-New: Animal Resource Ctr.",
	'ARC-Old': "ARC-Old: Animal Resource Ctr.",
	'CBA-N': "CBA-N: McCombs School of Business",
	'CBA-S': "CBA-S: McCombs School of Business",
	'POB': "ACE/POB: Peter O'Donnell Jr Building",
	'ADH': "ADH: Almetris Duren Hall",
	'AHG': "AHG: Anna Hiss Gymnasium",
	'ANB': "ANB: Arno Nowotny Building",
	'AND': "AND: Andrews Dormitory",
	'ART': "ART: Art bldg.",
	'ATT': "ATT: AT&T Executive Education and Conference Center",
	'BAT': "BAT: Batts Hall",
	'BEG': "BEG: Pickel Campus",
	'BEL': "BEL: L.Theo Bellmont Hall",
	'BEN': "BEN: Benedict Hall",
	'BHD': "BHD: Brackenridge Dorm.  BRD",
	'BIO': "BIO: Biological Lab.",
	'BLD': "BLD: Blanton Dormitory",
	'BMA': "BMA: Blanton Museum ofArt",
	'BMC': "BMC: BELO Center for New Media",
	'BME': "BME: Biomedical Engineering Bldg",
	'BRB': "BRB: Bernard and Audre Rapoport Building",
	'BRG': "BRG: Brazos Garage",
	'BTL': "BTL: Battle Hall",
	'BUR': "BUR: Burdine Hall",
	'BWY': "BWY: Bridgeway Building",
	'CAL': "CAL: Calhoun Hall",
	'CCJ': "CCJ: John B. Connally Center for Justice",
	'CDL': "CDL: Collections Deposit Library",
	'CLA': "CLA: Liberal Arts Building",
	'CMA': "CMA: Jesse H. Jones Communication Center - Building A",
	'CMB': "CMB: Jesse H. Jones Communication Center - Building B",
	'COM': "COM: Computation Center",
	'CP1': "CP1: PRC-Chilling Station #1",
	'CP2': "CP2: PRC-Chilling Station #2",
	'CPE': "CPE: Chemical Petroleum Bldg.CPE",
	'CRB': "CRB: Computational Resource Building",
	'CRD': "CRD: Carothers Dorm.",
	'CRH': "CRH: Creekside Residence Hall",
	'CS3': "CS3: Chill Station #3",
	'CS4': "CS4: Chill Station #4",
	'CS5': "CS5: Chill Station #5",
	'CS6': "CS6: Chill Station #6",
	'CS7': "CS7: Chilling Station #7",
	'DCP': "DCP: Denton Cooley Pavilion",
	'DFA': "DFA: E.William Doty Fine Art Bldg",
	'DFF': "DFF: Disch-Falk  Field",
	'EAS': "EAS: Edgar A. Smith Building",
	'ECG': "ECG: East Campus Garage",
	'ECJ': "ECJ: Ernest Cockrell Jr. Hall - Unit#1",
	'EME': "EME: Pickel Research Campus",
	'ENS': "ENS: Engineering-Science Building",
	'EPS': "EPS: E. P. Schoch Building",
	'ERC': "ERC: Frank Erwin Center",
	'ETC': "ETC: Engineering Teaching Ctr.",
	'FAC': "FAC: Peter Flawn Academic Center",
	'FC1': "FC1: Facilities DSMEC connection",
	'FNT': "FNT: Larry R. Faulkner Nano Science and Technology Building",
	'GAR': "GAR: Garrison Hall",
	'GEA': "GEA: Mary E Gearing Hall",
	'GEB': "GEB: Dorothy L. Gebauer Building",
	'GOL': "GOL: Goldsmith Hall",
	'GRG': "GRG: Geography Building                                                BLd  now GWB",
	'GSB': "GSB: Graduate School of Business Building",
	'HMA': "HMA: Hogg Memorial Auditorium",
	'HRC': "HRC: Harry Ransom Ctr.",
	'HRH': "HRH: Homer Rainey Hall",
	'HSM': "HSM: William Randolph Hearst Building",
	'HWP': "HWP: Hot Water Plant",
	'JGB': "JGB: Jackson Geological Sciences Building",
	'JGB': "JGB: Geology Bldg",
	'JHH': "JHH: John H. Jones",
	'JON': "JON: Jesse H Jones Hall",
	'KIN': "KIN: Kinsolving Dormitory",
	'LBJ': "LBJ: Lyndon B. Johnson Library",
	'LDH': "LDH: Longhorn Dining Hall",
	'LFH': "LFH: Littlefield Home",
	'LLA': "LLA: Living Learning Ctr. WCA",
	'LLB': "LLB: LLB Living Learning Center",
	'LLC': "LLC: LLC Living Learning Center",
	'LLD': "LLD: LLD Living Learning Center",
	'LLF': "LLF: LLF Living Learning Center",
	'LTD': "LTD: LittlefieldDorm.",
	'LTH': "LTH: Laboratory Theater Building",
	'MAG': "MAG: Manor Garage",
	'MAI': "MAI: Main Building",
	'MBB': "MBB: Louise and James Robert Moffett Molecular Biology Building",
	'MBE': "MBE: Music Building East",
	'MER': "MER: MER  { Pickel reserch Campus",
	'MEZ': "MEZ: Mezes-Benedict Hall",
	'MFH': "MFH: Richard Mithoff Track and Soccer Fieldhouse",
	'MHD': "MHD: Moore-Hill Hall Dormitory",
	'MMS': "MMS: Mike A. Myers Track and Soccer Fieldhouse",
	'MNC': "MNC: Moncrief- Neuhaus Athletic Center",
	'MRH': "MRH: Music Recital Hall",
	'NETL': "NETL: Pickel Research Campus",
	'NEZ': "NEZ: North End Zone",
	'NHB': "NHB: Norman Hackerman Building",
	'NMS': "NMS: Nanomolecular Science Building",
	'NOA': "NOA: North Office Building A",
	'NUR': "NUR: Nursing Bldg.",
	'PAC': "PAC: Performing Arts Center",
	'PAI-E': "PAI-E: Painter Hall East",
	'PAI-W': "PAI-W: Painter Hall West",
	'PAR': "PAR: Parlin Hall",
	'PAT': "PAT: Patterson",
	'PCL': "PCL: Perry Casteneda Library",
	'PHD': "PHD: Prather Hall Dormitory",
	'PPB': "PPB: Printing & Press Building",
	'PPE': "PPE: Hal C. Weaver Power Plant Expansion",
	'PPL': "PPL: Hal C. Weaver Power Plant",
	'RHD': "RHD: Robert H. Dorm.  RBD",
	'RLM': "RLM: Robert Lee Moore Bldg.",
	'ROC': "ROC: Pickel Research Campus",
	'RRH': "RRH: Robert B. Rowling Hall",
	'RSC': "RSC: Recreational Sport Ctr.",
	'SAC': "SAC: Student Activity Center",
	'SEA': "SEA: Sarah M. and Charles E. Seay Building",
	'SER': "SER: Service Bldg",
	'SJG': "SJG: San Jacinto Garage",
	'SJH': "SJH: San Jacinto Residence Hall",
	'SOF': "SOF: Satellite Operating Facility",
	'SRH': "SRH: Sid Richardson Hall",
	'SSB': "SSB: Student Service Bldg-Basement",
	'SSW': "SSW: School of Social Work Building",
	'STD': "STD: Darrell K Royal-Texas Memorial Stadium                       NEW      BEL",
	'SUT': "SUT: Sutton Hall",
	'SWG': "SWG: Speedway Garage",
	'SZB': "SZB: George L. Sanchez Building",
	'TCC': "TCC: Thompson Conference Center",
	'TES': "TES: Thermal Energy Storage Tank",
	'TMM': "TMM: Texas Memorial Museum",
	'TNH': "TNH: Townes Hall ( Law School )",
	'TRG': "TRG: Trinity Garage",
	'TSC': "TSC: Lee and Joe Jamail Texas Swimming Center",
	'TSG': "TSG: 27th Street Garage",
	'TTC': "TTC: Penick-Allison Tennis Ctr.",
	'UA9': "UA9: 2609 University Avenue",
	'UIL': "UIL: University Interscholastic League",
	'UNB': "UNB: Texas Union Building",
	'UPB': "UPB: University Police Building",
	'UTA': "UTA: UT Administration Building",
	'UTC': "UTC: University Teaching Center",
	'UTX': "UTX: Etter Harbin Alumni Ctr.",
	'WAG': "WAG: Waggener Hall",
	'WCH': "WCH: Will C. Hogg Building",
	'WEL': "WEL: Robert A. Welch Hall",
	'WIN': "WIN: F. Loren Winship Drama Building",
	'WMB': "WMB: West Mall Office Building",
	'WRW': "WRW: W.R.Woolrich Laboratories",
	'WWH': "WWH: Water Webb Hall (CMB bldg)",
};

function get_appsheet(building){
	var appsheet_building = building.toUpperCase(); // appsheet_buildings[building];
	var appsheet_building_encoded = encodeURI(appsheet_building);
	appsheet_building_encoded = appsheet_building_encoded.replace(/%20/g,'+');
	appsheet_building_encoded = appsheet_building_encoded.replace(/:/,'%3A');
	appsheet_building_encoded = appsheet_building_encoded.replace(/#/g,'%23');
	string_to_use = "https://www.appsheet.com/start/14615e71-754a-4c4c-9f81-b75606419b27#appName=UEM_EMO_BUMP-650229&group=%5B%5D&page=detail&row=" + appsheet_building_encoded + "&sort=%5B%7B%22Column%22%3A%22Building+Info%22%2C%22Order%22%3A%22Ascending%22%7D%5D&table=BUMP+PLCs&view=BUMP+PLCs_Detail";
	window.open(string_to_use);
}


function get_appsheets(){
	var records = $('#table_body > div');
	records.each(function(index, record){
		var record = $(record);
		if($(record.children().get(0)).children().prop("checked")){
			var building = record.find("[column=building]").text().toUpperCase();
			get_appsheet(building);
		}
	});
}

function export_to_file(){
	var export_data = "";
	export_data = export_data + "time" + ",";
	export_data = export_data + "building" + ",";
	export_data = export_data + "plc" + ",";
	export_data = export_data + "meter" + ",";
	export_data = export_data + "meter_type" + ",";
	export_data = export_data + "condition" + ",";
	export_data = export_data + "event_value" + ",";
	export_data = export_data + "alarming" + ",";
	export_data = export_data + "acknowledge";
	export_data = export_data + "\n";
	var records = $('#table_body > div');
	records.each(function(index, record){
		var record = $(record);
		if(record.is(":visible")){
			var time = record.find("div[column='time']").text();
			var building = record.find("div[column='building']").text();
			var plc = record.find("div[column='plc']").text();
			var meter = record.find("div[column='meter']").text();
			var meter_type = record.find("div[column='meter_type']").text();
			var condition = record.find("div[column='condition']").text();
			var event_value = record.find("div[column='event_value']").text();
			var alarming = record.find("div[column='alarming']").text();
			var acknowledge = record.find("div[column='acknowledge']").text();
			if(acknowledge == "press to acknowledge"){
				acknowledge = "(none)";
			}
			export_data = export_data + time + ",";
			export_data = export_data + building + ",";
			export_data = export_data + plc + ",";
			export_data = export_data + meter + ",";
			export_data = export_data + meter_type + ",";
			export_data = export_data + condition + ",";
			export_data = export_data + event_value + ",";
			export_data = export_data + alarming + ",";
			export_data = export_data + acknowledge;
			export_data = export_data + "\n";
		}
	});
	link = document.createElement("a");
    var blob = new Blob([export_data], { type: 'text/csv;charset=utf-8;' });
    var url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", "download.csv");
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
 
function addUser() {
  return true;
}

function update_acquisition_type_filter_array(selections_made) {
	acquisition_type_filter_array = selections_made.toUpperCase().split(",");
	filter_table();
}
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if ($(event.target).parents("#hamburger_menu").length == 0) { // clicked away from menu
    var openDropdown = document.getElementById("dropdown-content");
	if (openDropdown.classList.contains('show')) {
		openDropdown.classList.remove('show');
	}
  }
}